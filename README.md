# Mock Api
Application to replace a real api, creating request mocks to handle specific responses

## Contributions
Ideas and suggestions are really welcome! Vai sem medo!
Use the git hub issues[issues](https://github.com/Vizir/mock-api/issues) to manage these.

## Prerequisites

- [docker](https://www.docker.com/)
- [docker compose](https://docs.docker.com/compose/install/)
- [GNU Make](https://www.gnu.org/software/make/)

## Running

### Start application:

```bash
make up
```

This command will launch the app on: [http://localhost:3000](http://localhost:3000)

### View logs

```bash
make logs
```

## Coding

- [Install .editorconfig plugin on your editor](https://editorconfig.org/#download)

## Run npm commands
**Important:** all commands needs be executed in container bash terminal


To open container bash terminal
```bash
make console
```

## Npm Commands

### Lint
```bash
npm run lint

npm run lint:fix
```

### Format
```bash
npm run format
```
<small>Format code to prettier rules</small>

### Build
```bash
npm run build
```

#### Check-style
Run lint, format and build commands
```bash
npm run check-style
```

### Test
```bash
npm run test - run test suite

npm run test:watch - open tests with iterative watch to ts files

npm run test:coverage - collect coverage metrics
```

### Ci
Command used to CI pipeline to check code project

```bash
npm run ci
```

### Migrations
```
migrate:up - run pending migrations

migrate:undo - undo last migrations
```

## Pipelines

### CI
Use github actions <br>
```File: .github/workflows/ci.yml```

## Usage

### Custom Mocks Details
All the following arguments from the request are available to use in the js code in headers/body/status response:
```
headers
body
query
path
```
All this information will be available in the context. Example:
```
{
body: {},
headers: {
  host: 'vizir-mock-api.herokuapp.com',
  connection: 'close',
  'user-agent': 'PostmanRuntime/7.26.5',
  accept: '*/*',
  'postman-token': '5a3a2e08-3bbf-4f6e-a5e5-21a7179d98e3',
  'accept-encoding': 'gzip, deflate, br',
  'x-request-id': '8eed6849-2e64-4d04-9ddd-9731a490c431',
  'x-forwarded-for': '191.13.151.240',
  'x-forwarded-proto': 'https',
  'x-forwarded-port': '443',
  via: '1.1 vegur',
  'connect-time': '0',
  'x-request-start': '1641478391386',
  'total-route-time': '0',
  'x-tracking-id': '22f1a53e-13e3-4735-bccf-aa7063d5688f'
},
path: null,
query: { accountId: '2' }
}
```
By this example, you can use this by `this.query.accountId` to set some logic like:
```
if (this.query.accountId === "2") {
    return {200}
}
return {404}
```
* Be careful about types. In this case, 2 will be a string.
