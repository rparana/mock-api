import "reflect-metadata";
import { Server } from "@interfaces/http/server";

const PORT = parseInt(process.env.PORT ?? "3000");

Server.run().listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening: http://localhost:${PORT}`);
});
