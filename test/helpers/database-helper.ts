import type { EntitySchema } from "typeorm";
import type { PostgresDbConnection } from "@infrastructure/data/repositories/postgres/postgres-db-connection";
import { diTests } from "@test/di-tests";
import { DbConnection } from "@domain/contracts/repositories/db-connection";

class DatabaseHelper {
  private readonly dbConnection: PostgresDbConnection;

  public constructor() {
    this.dbConnection = diTests.get(DbConnection) as PostgresDbConnection;
  }

  public async clear(...mappings: EntitySchema[]): Promise<void> {
    const queryRunner = await this.dbConnection.getQueryRunner();

    for (const mapping of mappings) {
      await queryRunner.query(`truncate table ${mapping.options.name} cascade`);
    }
  }

  public async batch<T>(schema: EntitySchema<T>, ...values: T[]): Promise<void> {
    const connection = await this.dbConnection.getConnection();

    const builder = connection.createQueryBuilder();

    await builder.insert().into(schema).values(values).execute();
  }
}

const databaseHelper = new DatabaseHelper();
export { DatabaseHelper, databaseHelper };
