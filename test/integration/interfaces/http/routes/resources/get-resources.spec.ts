/* eslint-disable @typescript-eslint/no-magic-numbers */
import { StatusCodes } from "http-status-codes";
import type supertest from "supertest";
import { assert } from "chai";
import { serverFaker } from "@test/faker/server-fake";
import type { ResourcesDto } from "@application/entities/dtos/resources-dto";

describe("Integration: Routes", () => {
  describe("Resources: Get", () => {
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return accepted resources", async () => {
      // given
      // when
      // then
      await serverFaker
        .get("/manage/resources")
        .set(headers)
        .expect(StatusCodes.OK)
        .expect((res: supertest.Response) => {
          const { mimeTypes, httpMethods, statusCodes, mockResponseType } = res.body as ResourcesDto;

          // mimeTypes
          assert.lengthOf(mimeTypes, 3);
          assert.deepEqual(mimeTypes[0], "application/json");

          // httpMethods
          assert.lengthOf(httpMethods, 7);
          assert.deepEqual(httpMethods[0], "DELETE");

          // statusCodes
          assert.lengthOf(statusCodes, 55);
          assert.deepEqual(statusCodes[0].name, "CONTINUE");
          assert.deepEqual(statusCodes[0].status, StatusCodes.CONTINUE);

          // mockResponseType
          assert.lengthOf(mockResponseType, 2);
          assert.deepEqual(mockResponseType[0], "custom");
          assert.deepEqual(mockResponseType[1], "simple");
        });
    });
  });
});
