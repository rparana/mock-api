import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import { databaseHelper } from "@test/helpers/database-helper";
import { mockMapping, toMockDb } from "@infrastructure/data/repositories/postgres/mappings/mock-mapping";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";
import { projectFixture } from "@test/fixtures/project-fixture";
import { serverFaker } from "@test/faker/server-fake";
import { folderFixture } from "@test/fixtures/folder-fixture";
import { mockFixture } from "@test/fixtures/mock-fixture";
import type { MockDto } from "@application/entities/dtos/mock-dto";
import { diTests } from "@test/di-tests";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { mockResponseFixture } from "@test/fixtures/mock-response-fixture";
import type { PlainObject } from "@utils/types";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(mockMapping, folderMapping, projectMapping);
  });

  describe("Mock: Create", () => {
    const path = "/manage/mock/";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return error if project does not exist", async () => {
      // given
      const fakeProject = projectFixture.build();

      const mock = mockFixture.withProjectId(fakeProject.id).build();

      const body = {
        folderId: mock.id,
        method: mock.method,
        path: mock.path,
        projectId: mock.projectId,
        response: mock.response,
      };

      // when
      // then
      await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return error if folder does not exist", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const fakeFolder = folderFixture.withProjectId(project.id).build();

      const mock = mockFixture.withProjectId(project.id).build();

      const body = {
        folderId: fakeFolder.id,
        method: mock.method,
        path: mock.path,
        projectId: mock.projectId,
        response: mock.response,
      };

      // when
      // then
      await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });

    it("Should return error if mock already exists for that project", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const existingMock = mockFixture.withProjectId(project.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(existingMock));

      const body = {
        folderId: null,
        method: existingMock.method,
        path: existingMock.path,
        projectId: existingMock.projectId,
        response: existingMock.response,
      };

      // when
      // then
      await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });

    it("Should create mock without folder", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const mock = mockFixture.withProjectId(project.id).build();

      const body = {
        folderId: null,
        method: mock.method,
        path: mock.path,
        projectId: mock.projectId,
        response: mock.response,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<MockDto>;

      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.active, true);
      assert.deepEqual(dto.folderId, null);
      assert.deepEqual(dto.method, mock.method);
      assert.deepEqual(dto.path, mock.path);
      assert.deepEqual(dto.projectId, mock.projectId);
      assert.deepEqual(dto.response, mock.response);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(dto.id);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.active, true);
      assert.deepEqual(saved?.folderId, null);
      assert.deepEqual(saved?.method, mock.method);
      assert.deepEqual(saved?.path, mock.path);
      assert.deepEqual(saved?.projectId, mock.projectId);
      assert.deepEqual(saved?.response, mock.response);
    });

    it("Should create mock with a folder", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const mock = mockFixture.withProjectId(project.id).build();

      const body = {
        folderId: folder.id,
        method: mock.method,
        path: mock.path,
        projectId: mock.projectId,
        response: mock.response,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<MockDto>;

      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.active, true);
      assert.deepEqual(dto.folderId, folder.id);
      assert.deepEqual(dto.method, mock.method);
      assert.deepEqual(dto.path, mock.path);
      assert.deepEqual(dto.projectId, mock.projectId);
      assert.deepEqual(dto.response, mock.response);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(dto.id);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.active, true);
      assert.deepEqual(saved?.folderId, folder.id);
      assert.deepEqual(saved?.method, mock.method);
      assert.deepEqual(saved?.path, mock.path);
      assert.deepEqual(saved?.projectId, mock.projectId);
      assert.deepEqual(saved?.response, mock.response);
    });

    it("Should create mock with a custom response", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const mock = mockFixture.withProjectId(project.id).build();
      const customResponse = mockResponseFixture.buildCustom();

      const body = {
        folderId: null,
        method: mock.method,
        path: mock.path,
        projectId: mock.projectId,
        response: customResponse,
      };
      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<MockDto>;

      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.active, true);
      assert.deepEqual(dto.folderId, null);
      assert.deepEqual(dto.method, mock.method);
      assert.deepEqual(dto.path, mock.path);
      assert.deepEqual(dto.projectId, mock.projectId);
      assert.deepEqual(dto.response, customResponse);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(dto.id);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.active, true);
      assert.deepEqual(saved?.folderId, null);
      assert.deepEqual(saved?.method, mock.method);
      assert.deepEqual(saved?.path, mock.path);
      assert.deepEqual(saved?.projectId, mock.projectId);
      assert.deepEqual(saved?.response, customResponse);
    });

    it("Should create the same mock for another project", async () => {
      // given
      const currentProject = projectFixture.build();
      const otherProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, currentProject, otherProject);

      const mock = mockFixture.withProjectId(otherProject.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(mock));

      const body = {
        folderId: null,
        method: mock.method,
        path: mock.path,
        projectId: currentProject.id,
        response: mock.response,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<MockDto>;

      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.active, true);
      assert.deepEqual(dto.folderId, null);
      assert.deepEqual(dto.method, mock.method);
      assert.deepEqual(dto.path, mock.path);
      assert.deepEqual(dto.projectId, currentProject.id);
      assert.deepEqual(dto.response, mock.response);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(dto.id);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.active, true);
      assert.deepEqual(saved?.folderId, null);
      assert.deepEqual(saved?.method, mock.method);
      assert.deepEqual(saved?.path, mock.path);
      assert.deepEqual(saved?.projectId, currentProject.id);
      assert.deepEqual(saved?.response, mock.response);
    });
  });
});
