import { assert } from "chai";
import { StatusCodes } from "http-status-codes";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";
import { mockMapping, toMockDb } from "@infrastructure/data/repositories/postgres/mappings/mock-mapping";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { diTests } from "@test/di-tests";
import { serverFaker } from "@test/faker/server-fake";
import { mockFixture } from "@test/fixtures/mock-fixture";
import { mockResponseFixture } from "@test/fixtures/mock-response-fixture";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(mockMapping, folderMapping, projectMapping);
  });

  describe("Mock: Edit", () => {
    const basePath = "/manage/mock/:mockId";
    const replaceMockId = ":mockId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return error if mock does not exist", async () => {
      // given
      const fakeMock = mockFixture.build();

      const path = basePath.replace(replaceMockId, fakeMock.id);

      const newMockValues = mockFixture.build();
      const body = {
        method: newMockValues.method,
        path: newMockValues.path,
        response: newMockValues.response,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return error if there is another mock with the same path and method for a project", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const currentMock = mockFixture.withProjectId(project.id).build();
      const otherMock = mockFixture.withProjectId(project.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(currentMock), toMockDb(otherMock));

      const path = basePath.replace(replaceMockId, currentMock.id);

      const body = {
        method: otherMock.method,
        path: otherMock.path,
        response: mockResponseFixture.buildSimple(),
      };
      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });

    it("Should edit a mock", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const mock = mockFixture.withProjectId(project.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(mock));

      const path = basePath.replace(replaceMockId, mock.id);

      const newMockValues = mockFixture.build();
      const body = {
        method: newMockValues.method,
        path: newMockValues.path,
        response: newMockValues.response,
      };
      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.OK);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(mock.id);

      assert.deepEqual(saved?.id, mock.id);
      assert.deepEqual(saved?.active, mock.active);
      assert.deepEqual(saved?.projectId, mock.projectId);
      assert.deepEqual(saved?.folderId, mock.folderId);
      assert.deepEqual(saved?.method, body.method);
      assert.deepEqual(saved?.path, body.path);
      assert.deepEqual(saved?.response, body.response);
    });

    it("Should edit a mock even with other project mock values", async () => {
      // given
      const currentProject = projectFixture.build();
      const otherProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, currentProject, otherProject);

      const currentMock = mockFixture.withProjectId(currentProject.id).build();
      const otherProjectMock = mockFixture.withProjectId(otherProject.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(currentMock), toMockDb(otherProjectMock));

      const path = basePath.replace(replaceMockId, currentMock.id);

      const body = {
        method: otherProjectMock.method,
        path: otherProjectMock.path,
        response: otherProjectMock.response,
      };
      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.OK);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(currentMock.id);

      assert.deepEqual(saved?.id, currentMock.id);
      assert.deepEqual(saved?.active, currentMock.active);
      assert.deepEqual(saved?.projectId, currentMock.projectId);
      assert.deepEqual(saved?.folderId, currentMock.folderId);
      assert.deepEqual(saved?.method, body.method);
      assert.deepEqual(saved?.path, body.path);
      assert.deepEqual(saved?.response, body.response);
    });
  });
});
