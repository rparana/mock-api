import { assert } from "chai";
import { StatusCodes } from "http-status-codes";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { mockMapping, toMockDb } from "@infrastructure/data/repositories/postgres/mappings/mock-mapping";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { diTests } from "@test/di-tests";
import { serverFaker } from "@test/faker/server-fake";
import { mockFixture } from "@test/fixtures/mock-fixture";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(mockMapping, projectMapping);
  });

  describe("Mock: Deactivate", () => {
    const basePath = "/manage/mock/:mockId/deactivate";
    const replaceMockId = ":mockId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return error if mock does not exist", async () => {
      // given
      const fakeMock = mockFixture.build();

      const path = basePath.replace(replaceMockId, fakeMock.id);

      const body = {
        active: false,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should deactivate a mock", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const mock = mockFixture.withProjectId(project.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(mock));

      const path = basePath.replace(replaceMockId, mock.id);

      const body = {
        active: false,
      };
      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.OK);

      const repository = diTests.get(MockRepository);

      const saved = await repository.findById(mock.id);

      assert.deepEqual(saved?.id, mock.id);
      assert.deepEqual(saved?.active, body.active);
    });
  });
});
