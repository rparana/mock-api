import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import { serverFaker } from "@test/faker/server-fake";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { mockFixture } from "@test/fixtures/mock-fixture";
import { mockMapping, toMockDb } from "@infrastructure/data/repositories/postgres/mappings/mock-mapping";
import { diTests } from "@test/di-tests";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(mockMapping, projectMapping);
  });

  describe("Mock: Delete", () => {
    const basePath = "/manage/project/:projectId/mock/:mockId";
    const replaceMockId = ":mockId";
    const replaceProjectId = ":projectId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should delete a mock", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const mock = mockFixture.withProjectId(project.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(mock));

      const path = basePath.replace(replaceMockId, mock.id).replace(replaceProjectId, project.id);

      // when
      // then
      await serverFaker.delete(path).set(headers).expect(StatusCodes.OK);

      const mockRepository = diTests.get(MockRepository);

      const deletedMock = await mockRepository.findById(mock.id);

      assert.isNull(deletedMock);
    });
  });
});
