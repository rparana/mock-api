import { name, lorem } from "faker";
import { StatusCodes } from "http-status-codes";
import type supertest from "supertest";
import { assert } from "chai";
import { plainToClass } from "class-transformer";
import { serverFaker } from "@test/faker/server-fake";
import { diTests } from "@test/di-tests";
import { projectFixture } from "@test/fixtures/project-fixture";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { databaseHelper } from "@test/helpers/database-helper";
import { ValidationError } from "@domain/validation/validation-error";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { folderFixture } from "@test/fixtures/folder-fixture";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";

const REPEAT_TIMES = 40;

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(folderMapping, projectMapping);
  });

  describe("Folder: Edit", () => {
    const basePath = "/manage/folder/:folderId";
    const replaceFolderId = ":folderId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return error if project does not exist", async () => {
      // given
      const fakeProject = projectFixture.build();
      const folder = folderFixture.build();
      const path = basePath.replace(replaceFolderId, folder.id);
      const body = {
        name: `${name.firstName().toLowerCase()}_${new Date().getFullYear()}`,
        projectId: fakeProject.id,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return error if folder does not exist", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.build();

      const path = basePath.replace(replaceFolderId, folder.id);
      const body = {
        name: `${name.firstName().toLowerCase()}_${new Date().getFullYear()}`,
        projectId: project.id,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return error if folder name is invalid", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const path = basePath.replace(replaceFolderId, folder.id);
      const body = {
        name: `${lorem.text().repeat(REPEAT_TIMES)}`,
        projectId: project.id,
      };

      // when
      // then
      await serverFaker
        .put(path)
        .set(headers)
        .send(body)
        .expect(StatusCodes.UNPROCESSABLE_ENTITY)
        .expect((res: supertest.Response) => {
          const validation = plainToClass(ValidationError, res.body as unknown[]);

          const EXPECTED_ERRORS = 1;
          assert.lengthOf(validation, EXPECTED_ERRORS);

          const [invalidLength] = validation;

          assert.deepEqual(invalidLength.value, body.name);
          assert.deepEqual(invalidLength.property, "name");
          assert.deepEqual(invalidLength.validation?.isLengthGreaterThan, "name length must not be greater than 40");
        });
    });

    it("Should return conflict if name already in use for that project", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const currentFolder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, currentFolder);
      const otherFolder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, otherFolder);

      const path = basePath.replace(replaceFolderId, currentFolder.id);
      const body = {
        name: otherFolder.name,
        projectId: project.id,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });

    it("Should edit folder name even that the name is already in use in another project folder", async () => {
      // given
      const currentProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, currentProject);
      const otherProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, otherProject);

      const currentFolder = folderFixture.withProjectId(currentProject.id).build();
      await databaseHelper.batch(folderMapping, currentFolder);
      const otherProjectFolder = folderFixture.withProjectId(otherProject.id).build();
      await databaseHelper.batch(folderMapping, otherProjectFolder);

      const path = basePath.replace(replaceFolderId, currentFolder.id);
      const body = {
        name: otherProjectFolder.name,
        projectId: currentProject.id,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.OK);
    });

    it("Should edit a folder", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const repository = diTests.get(FolderRepository);

      const path = basePath.replace(replaceFolderId, folder.id);
      const body = {
        name: `${name.firstName().toLowerCase()}_${new Date().getFullYear()}`,
        projectId: project.id,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.OK);

      const saved = await repository.findById(folder.id);

      assert.deepEqual(saved?.id, folder.id);
      assert.deepEqual(saved?.name, body.name);
    });
  });
});
