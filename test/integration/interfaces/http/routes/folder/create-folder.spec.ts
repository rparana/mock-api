import { name, lorem } from "faker";
import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import { plainToClass } from "class-transformer";
import { serverFaker } from "@test/faker/server-fake";
import { diTests } from "@test/di-tests";
import { projectFixture } from "@test/fixtures/project-fixture";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { databaseHelper } from "@test/helpers/database-helper";
import { ValidationError } from "@domain/validation/validation-error";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import type { FolderDto } from "@application/entities/dtos/folder-dto";
import { folderFixture } from "@test/fixtures/folder-fixture";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";
import type { PlainObject } from "@utils/types";

const REPEAT_TIMES = 40;
describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(folderMapping, projectMapping);
  });

  describe("Folder: Create", () => {
    const path = "/manage/folder";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return error if project does not exist", async () => {
      // given
      const fakeProject = projectFixture.build();

      const body = {
        name: `${lorem.text().repeat(REPEAT_TIMES)}`,
        projectId: fakeProject.id,
      };

      // when
      // then
      await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return conflict if name already in use for that project", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const body = {
        name: folder.name,
        projectId: project.id,
      };

      // when
      // then
      await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });

    it("Should return error if folder name is invalid", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const body = {
        name: `${lorem.text().repeat(REPEAT_TIMES)}`,
        projectId: project.id,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.UNPROCESSABLE_ENTITY);

      const validation = plainToClass(ValidationError, res.body as unknown[]);

      const EXPECTED_ERRORS = 1;
      assert.lengthOf(validation, EXPECTED_ERRORS);

      const [invalidLength] = validation;

      assert.deepEqual(invalidLength.value, body.name);
      assert.deepEqual(invalidLength.property, "name");
      assert.deepEqual(invalidLength.validation?.isLengthGreaterThan, "name length must not be greater than 40");
    });

    it("Should create new folder with the same name used by other project folder", async () => {
      // given
      const otherProject = projectFixture.build();
      const currentProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, otherProject, currentProject);

      const folder = folderFixture.withProjectId(otherProject.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const body = {
        name: folder.name,
        projectId: currentProject.id,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<FolderDto>;

      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.projectId, currentProject.id);
      assert.deepEqual(dto.name, body.name);

      const repository = diTests.get(FolderRepository);

      const saved = await repository.find(currentProject.id, body.name);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.projectId, dto.projectId);
      assert.deepEqual(saved?.name, body.name);
    });

    it("Should create new folder", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const body = {
        name: `${name.firstName().toLowerCase()}_${new Date().getFullYear()}`,
        projectId: project.id,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<FolderDto>;
      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.projectId, project.id);
      assert.deepEqual(dto.name, body.name);

      const repository = diTests.get(FolderRepository);

      const saved = await repository.find(project.id, body.name);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.projectId, dto.projectId);
      assert.deepEqual(saved?.name, body.name);
    });
  });
});
