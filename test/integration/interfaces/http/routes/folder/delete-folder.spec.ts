import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import { serverFaker } from "@test/faker/server-fake";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { folderFixture } from "@test/fixtures/folder-fixture";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";
import { diTests } from "@test/di-tests";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(folderMapping, projectMapping);
  });

  describe("Folder: Delete", () => {
    const basePath = "/manage/project/:projectId/folder/:folderId";
    const replaceFolderId = ":folderId";
    const replaceProjectId = ":projectId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should delete a folder", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const path = basePath.replace(replaceFolderId, folder.id).replace(replaceProjectId, project.id);

      // when
      // then
      await serverFaker.delete(path).set(headers).expect(StatusCodes.OK);

      const folderRepository = diTests.get(FolderRepository);

      const deletedFolder = await folderRepository.findById(folder.id);

      assert.isNull(deletedFolder);
    });
  });
});
