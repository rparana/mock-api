import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import type supertest from "supertest";
import { plainToClass } from "class-transformer";
import { serverFaker } from "@test/faker/server-fake";
import { diTests } from "@test/di-tests";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";
import { ValidationError } from "@domain/validation/validation-error";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(projectMapping);
  });

  describe("Project: Edit", () => {
    const basePath = `/manage/project`;
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return not found if project does not exist", async () => {
      // given
      const unexistingProject = projectFixture.build();
      const path = `${basePath}/${unexistingProject.id}`;

      const body = {
        name: `${unexistingProject.name}Test`,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return error if invalid project name", async () => {
      // given
      const existingProject = projectFixture.build();

      await databaseHelper.batch(projectMapping, existingProject);

      const path = `${basePath}/${existingProject.id}`;

      const body = {
        name: `${existingProject.name}Test`,
      };

      // when
      // then
      await serverFaker
        .put(path)
        .set(headers)
        .send(body)
        .expect(StatusCodes.UNPROCESSABLE_ENTITY)
        .expect((res: supertest.Response) => {
          const validation = plainToClass(ValidationError, res.body as unknown[]);

          const EXPECTED_ERRORS = 1;
          assert.lengthOf(validation, EXPECTED_ERRORS);

          const [invalidName] = validation;

          assert.deepEqual(invalidName.value, body.name);
          assert.deepEqual(invalidName.property, "name");
          assert.deepEqual(
            invalidName.validation?.projectName,
            "name accepts lowercase letters, numbers or _ (underline)"
          );
        });
    });

    it("Should return conflict if name already in use", async () => {
      // given
      const existingProject = projectFixture.build();
      const duplicatedNameProject = projectFixture.build();

      await databaseHelper.batch(projectMapping, existingProject, duplicatedNameProject);

      const path = `${basePath}/${existingProject.id}`;

      const body = {
        name: duplicatedNameProject.name,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });

    it("Should edit a project", async () => {
      // given
      const existingProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, existingProject);

      const repository = diTests.get(ProjectRepository);
      const path = `${basePath}/${existingProject.id}`;

      const body = {
        name: `${existingProject.name}_test`,
      };

      // when
      // then
      await serverFaker.put(path).set(headers).send(body).expect(StatusCodes.OK);

      const saved = await repository.findById(existingProject.id);

      assert.deepEqual(saved?.id, existingProject.id);
      assert.deepEqual(saved?.name, body.name.toLowerCase());
    });
  });
});
