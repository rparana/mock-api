import { assert } from "chai";
import { StatusCodes } from "http-status-codes";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";
import { mockMapping, toMockDb } from "@infrastructure/data/repositories/postgres/mappings/mock-mapping";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { serverFaker } from "@test/faker/server-fake";
import { folderFixture } from "@test/fixtures/folder-fixture";
import { mockFixture } from "@test/fixtures/mock-fixture";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(projectMapping, folderMapping, mockMapping);
  });

  describe("Project: Get Project Mocks", () => {
    const basePath = "/manage/project/:projectId";
    const replaceProjectId = ":projectId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return return error if project does not exist", async () => {
      // given
      const fakeProject = projectFixture.build();
      const path = basePath.replace(replaceProjectId, fakeProject.id);
      // when
      // then
      await serverFaker.get(path).set(headers).expect(StatusCodes.NOT_FOUND);
    });

    it("Should return all project folders and mocks", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const folder = folderFixture.withProjectId(project.id).build();
      await databaseHelper.batch(folderMapping, folder);

      const mock = mockFixture.withProjectId(project.id).build();
      const inFolderMock = mockFixture.withProjectId(project.id).withFolderId(folder.id).build();
      await databaseHelper.batch(mockMapping, toMockDb(mock), toMockDb(inFolderMock));

      const path = basePath.replace(replaceProjectId, project.id);
      // when
      // then
      const result = await serverFaker.get(path).set(headers).expect(StatusCodes.OK);
      const expectedBody = {
        folders: [
          {
            id: folder.id,
            mocks: [
              {
                active: inFolderMock.active,
                folderId: folder.id,
                id: inFolderMock.id,
                method: inFolderMock.method,
                path: inFolderMock.path,
                projectId: inFolderMock.projectId,
                response: {
                  body: inFolderMock.response.body,
                  headers: inFolderMock.response.headers,
                  mimeType: inFolderMock.response.mimeType,
                  status: inFolderMock.response.status,
                  type: inFolderMock.response.type,
                },
              },
            ],
            name: folder.name,
            projectId: project.id,
          },
        ],
        id: project.id,
        mocks: [
          {
            active: mock.active,
            folderId: null,
            id: mock.id,
            method: mock.method,
            path: mock.path,
            projectId: mock.projectId,
            response: {
              body: mock.response.body,
              headers: mock.response.headers,
              mimeType: mock.response.mimeType,
              status: mock.response.status,
              type: mock.response.type,
            },
          },
        ],
        name: project.name,
      };
      assert.deepEqual(result.body, expectedBody);
    });
  });
});
