import { assert } from "chai";
import { StatusCodes } from "http-status-codes";
import type supertest from "supertest";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { serverFaker } from "@test/faker/server-fake";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";
import type { PlainObject } from "@utils/types";
import type { ProjectDto } from "@application/entities/dtos/project-dto";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(projectMapping);
  });

  describe("Project: Get", () => {
    const path = "/manage/project";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return all projects ordered by name asc", async () => {
      // given
      const projectA = projectFixture.withName(`A`).build();
      const projectB = projectFixture.withName(`B`).build();

      await databaseHelper.batch(projectMapping, projectB, projectA);

      // when
      // then
      await serverFaker
        .get(path)
        .set(headers)
        .expect(StatusCodes.OK)
        .expect((res: supertest.Response) => {
          const projects = res.body as PlainObject<ProjectDto>[];

          assert.deepEqual(projects[0].id, projectA.id);
          assert.deepEqual(projects[0].name, projectA.name);
          assert.deepEqual(projects[1].id, projectB.id);
          assert.deepEqual(projects[1].name, projectB.name);
        });
    });
  });
});
