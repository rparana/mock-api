import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import { serverFaker } from "@test/faker/server-fake";
import { diTests } from "@test/di-tests";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { folderFixture } from "@test/fixtures/folder-fixture";
import { folderMapping } from "@infrastructure/data/repositories/postgres/mappings/folder-mapping";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";

describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(projectMapping);
  });

  describe("Project: Delete", () => {
    const basePath = "/manage/project/:projectId";
    const replaceProject = ":projectId";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should delete project without folder", async () => {
      // given
      const existingProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, existingProject);
      const path = basePath.replace(replaceProject, existingProject.id);
      // when
      // then
      await serverFaker.delete(path).set(headers).expect(StatusCodes.OK);

      const repository = diTests.get(ProjectRepository);

      const deleted = await repository.findById(existingProject.id);

      assert.isNull(deleted);
    });

    it("Should delete project and its folders", async () => {
      // given
      const existingProject = projectFixture.build();
      await databaseHelper.batch(projectMapping, existingProject);

      const existingFolder = folderFixture.withProjectId(existingProject.id).build();
      await databaseHelper.batch(folderMapping, existingFolder);

      const path = basePath.replace(replaceProject, existingProject.id);
      // when
      // then
      await serverFaker.delete(path).set(headers).expect(StatusCodes.OK);

      const projectRepository = diTests.get(ProjectRepository);
      const folderRepository = diTests.get(FolderRepository);

      const deletedProject = await projectRepository.findById(existingProject.id);
      const deletedfolder = await folderRepository.findById(existingFolder.id);

      assert.isNull(deletedProject);
      assert.isNull(deletedfolder);
    });
  });
});
