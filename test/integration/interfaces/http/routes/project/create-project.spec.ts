import { name, lorem, datatype } from "faker";
import { StatusCodes } from "http-status-codes";
import { assert } from "chai";
import { plainToClass } from "class-transformer";
import { serverFaker } from "@test/faker/server-fake";
import type { ProjectDto } from "@application/entities/dtos/project-dto";
import { diTests } from "@test/di-tests";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { projectFixture } from "@test/fixtures/project-fixture";
import { databaseHelper } from "@test/helpers/database-helper";
import { ValidationError } from "@domain/validation/validation-error";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import type { PlainObject } from "@utils/types";

const REPEAT_TIMES = 40;
describe("Integration: Routes", () => {
  beforeEach(async () => {
    await databaseHelper.clear(projectMapping);
  });

  describe("Project: Create", () => {
    const path = "/manage/project";
    const headers = { authorization: "bearer fakeAccessToken" };

    it("Should return invalid payload", async () => {
      // given
      const body = {
        name: datatype.number(),
      };

      // when
      // then
      await serverFaker
        .post(path)
        .set(headers)
        .send(body)
        .expect(StatusCodes.UNPROCESSABLE_ENTITY, [
          {
            children: [],
            property: "name",
            validation: {
              isString: "name must be a string",
            },
            value: body.name,
          },
        ]);
    });

    it("Should create new project", async () => {
      // given
      const body = {
        name: `${name.firstName().toLowerCase()}_${new Date().getFullYear()}`,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CREATED);

      const dto = res.body as PlainObject<ProjectDto>;
      assert.isNotEmpty(dto.id);
      assert.deepEqual(dto.name, body.name);

      const repository = diTests.get(ProjectRepository);

      const saved = await repository.find(body.name);

      assert.deepEqual(saved?.id, dto.id);
      assert.deepEqual(saved?.name, body.name);
    });

    it("Should return error if project name is invalid", async () => {
      // given
      const body = {
        name: `${lorem.text().repeat(REPEAT_TIMES)}_$`,
      };

      // when
      // then
      const res = await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.UNPROCESSABLE_ENTITY);

      const validation = plainToClass(ValidationError, res.body as unknown[]);

      const EXPECTED_ERRORS = 2;
      assert.lengthOf(validation, EXPECTED_ERRORS);

      const [invalidName, invalidLength] = validation;

      assert.deepEqual(invalidName.value, body.name);
      assert.deepEqual(invalidName.property, "name");
      assert.deepEqual(invalidName.validation?.projectName, "name accepts lowercase letters, numbers or _ (underline)");

      assert.deepEqual(invalidLength.value, body.name);
      assert.deepEqual(invalidLength.property, "name");
      assert.deepEqual(invalidLength.validation?.isLengthGreaterThan, "name length must not be greater than 40");
    });

    it("Should return conflict if name already in use", async () => {
      // given
      const existingProject = projectFixture.build();

      await databaseHelper.batch(projectMapping, existingProject);

      const body = {
        name: existingProject.name,
      };

      // when
      // then
      await serverFaker.post(path).set(headers).send(body).expect(StatusCodes.CONFLICT);
    });
  });
});
