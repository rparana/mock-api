import { assert } from "chai";
import { lorem } from "faker";
import { StatusCodes } from "http-status-codes";
import { serverFaker } from "@test/faker/server-fake";
import type { PlainObject } from "@utils/types";
import type { TokenDto } from "@domain/contracts/services/token-dto";

describe("Integration: Routes", () => {
  describe("Auth: Get Access Token", () => {
    const path = "/google/authenticated";
    const TOKEN_EXPIRATION = 3600;

    it("Should return the access token", async () => {
      // given
      const queryString = {
        code: lorem.text(),
        redirectUrl: `https://${lorem.text()}.com.br`,
      };

      await serverFaker
        .get(path)
        .query(queryString)
        .expect(StatusCodes.OK)
        .expect((res) => {
          const responseBody = res.body as PlainObject<TokenDto>;

          assert.equal(responseBody.accessToken, "fakeAccessToken");
          assert.equal(responseBody.refreshToken, "refreshToken");
          assert.equal(responseBody.tokenType, "Bearer");
          assert.equal(responseBody.expiresIn, TOKEN_EXPIRATION);
        });
    });
  });
});
