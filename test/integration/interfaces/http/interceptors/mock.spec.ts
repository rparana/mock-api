import { StatusCodes } from "http-status-codes";
import { datatype } from "faker";
import { assert } from "chai";
import { mockMapping, toMockDb } from "@infrastructure/data/repositories/postgres/mappings/mock-mapping";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { databaseHelper } from "@test/helpers/database-helper";
import { serverFaker } from "@test/faker/server-fake";
import { projectFixture } from "@test/fixtures/project-fixture";
import { MockResponseType } from "@domain/enums/mock-response-type";
import { MockResponse } from "@domain/value-objects/mock-response";
import { MimeTypes } from "@domain/enums/mime-types";
import { Mock } from "@domain/entities/mock";
import { HttpMethod } from "@domain/enums/http-methods";
import type { ExecutionError } from "@domain/value-objects/execution-result";

describe("Integration: Interceptors", () => {
  describe("Mock", () => {
    beforeEach(async () => {
      await databaseHelper.clear(projectMapping, mockMapping);
    });

    it("Should return 404 if project not found", async () => {
      // given
      const url = "/mock/fake/v1/credit-card";

      // when
      // then
      await serverFaker.get(url).send().expect(StatusCodes.NOT_FOUND);
    });

    it("Should return 404 if mock not found", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const url = `/mock/${project.name}/test`;

      // when
      // then
      await serverFaker.get(url).send().expect(StatusCodes.NOT_FOUND);
    });

    describe("simple", () => {
      it("Should return a simple response", async () => {
        // given
        const project = projectFixture.build();
        await databaseHelper.batch(projectMapping, project);

        const myHeader = datatype.uuid();
        const response = new MockResponse(
          MockResponseType.simple,
          StatusCodes.PARTIAL_CONTENT.toString(),
          MimeTypes.text,
          "PARTIAL TEXT BODY",
          JSON.stringify({ "my-header": myHeader })
        );

        const mock = new Mock(project.id, null, HttpMethod.get, "/test", response);
        await databaseHelper.batch(mockMapping, toMockDb(mock));

        // when
        // then
        await serverFaker
          .get(`/mock/${project.name}/test`)
          .send()
          .expect(StatusCodes.PARTIAL_CONTENT)
          .expect((res) => {
            assert.equal(res.text, response.body);

            const headers = res.headers as Record<string, string>;
            assert.equal(headers["my-header"], myHeader);
            assert.equal(headers["content-type"], `${MimeTypes.text}; charset=utf-8`);
          });
      });

      it("Should return a simple mock with just status", async () => {
        // given
        const project = projectFixture.build();
        await databaseHelper.batch(projectMapping, project);

        const response = new MockResponse(MockResponseType.simple, StatusCodes.NO_CONTENT.toString(), MimeTypes.json);

        const mock = new Mock(project.id, null, HttpMethod.get, "/test", response);
        await databaseHelper.batch(mockMapping, toMockDb(mock));

        // when
        // then
        await serverFaker.get(`/mock/${project.name}/test`).send().expect(StatusCodes.NO_CONTENT);
      });
    });

    describe("custom", () => {
      it("Should return error to process custom response", async () => {
        // given
        const project = projectFixture.build();
        await databaseHelper.batch(projectMapping, project);

        const paramBody = {
          id: 20,
        };

        const myHeader = datatype.uuid();
        const response = new MockResponse(
          MockResponseType.custom,
          "if (this.body.id === 10) { return 201 } return 409",
          MimeTypes.text,
          "PARTIAL TEXT BODY",
          JSON.stringify({ "my-header": myHeader })
        );

        const mock = new Mock(project.id, null, HttpMethod.post, "/test", response);
        await databaseHelper.batch(mockMapping, toMockDb(mock));

        // when
        // then
        await serverFaker
          .post(`/mock/${project.name}/test?customerId=1`)
          .send(paramBody)
          .expect(StatusCodes.UNPROCESSABLE_ENTITY)
          .expect((res) => {
            const responseBody = res.body as ExecutionError;

            assert.isFalse(responseBody.success);
            assert.equal(responseBody.code, response.body);
            assert.equal(responseBody.error, "SyntaxError: Unexpected identifier");
            assert.equal(responseBody.property, "body");

            assert.deepEqual(responseBody.args.body, paramBody);
            assert.isNull(responseBody.args.path);
            assert.deepEqual(responseBody.args.query, {
              customerId: "1",
            });
            assert.isNotNull(responseBody.args.headers);
          });
      });

      it("Should return a custom response", async () => {
        // given
        const project = projectFixture.build();
        await databaseHelper.batch(projectMapping, project);

        const response = new MockResponse(
          MockResponseType.custom,
          "if (this.body.id === 10) { return 417 } return 402",
          MimeTypes.json,
          null,
          "return { status: 417 }"
        );

        const mock = new Mock(project.id, null, HttpMethod.post, "/test", response);
        await databaseHelper.batch(mockMapping, toMockDb(mock));

        // when
        // then
        await serverFaker
          .post(`/mock/${project.name}/test`)
          .send({
            id: 10,
          })
          .expect(StatusCodes.EXPECTATION_FAILED)
          .expect((res) => {
            const headers = res.header as Record<string, string>;

            assert.equal(headers["content-type"], `${MimeTypes.json}`);
            assert.equal(headers["status"], StatusCodes.EXPECTATION_FAILED.toString());
          });
      });

      it("Should return error if status code result is invalid", async () => {
        // given
        const project = projectFixture.build();
        await databaseHelper.batch(projectMapping, project);

        const response = new MockResponse(MockResponseType.custom, "200", MimeTypes.json, "", "");

        const mock = new Mock(project.id, null, HttpMethod.post, "/test", response);
        await databaseHelper.batch(mockMapping, toMockDb(mock));

        // when
        // then
        await serverFaker
          .post(`/mock/${project.name}/test`)
          .send()
          .expect(StatusCodes.UNPROCESSABLE_ENTITY)
          .expect((res) => {
            const responseBody = res.body as ExecutionError;

            assert.isFalse(responseBody.success);
            assert.equal(responseBody.property, "status");
            assert.equal(responseBody.code, response.status);

            assert.deepEqual(responseBody.args.body, {});
            assert.isNull(responseBody.args.path);
            assert.deepEqual(responseBody.args.query, {});
            assert.isNotNull(responseBody.args.headers);

            assert.equal(responseBody.error, "Code dont return a valid http status: undefined");
          });
      });
    });

    describe("path", () => {
      it("Should return error from a mock with path variables", async () => {
        // given
        const project = projectFixture.build();
        await databaseHelper.batch(projectMapping, project);

        const response = new MockResponse(
          MockResponseType.custom,
          `return ${StatusCodes.PARTIAL_CONTENT.toString()}`,
          MimeTypes.xml,
          "<envelope></envelope>"
        );

        const path = {
          accountId: datatype.uuid(),
          customerId: datatype.uuid(),
        };

        const mock = new Mock(project.id, null, HttpMethod.get, "/:customerId/test/:accountId", response);
        await databaseHelper.batch(mockMapping, toMockDb(mock));

        // when
        // then
        await serverFaker
          .get(`/mock/${project.name}/${path.customerId}/test/${path.accountId}`)
          .send()
          .expect(StatusCodes.UNPROCESSABLE_ENTITY)
          .expect((res) => {
            const responseBody = res.body as ExecutionError;

            assert.isFalse(responseBody.success);
            assert.equal(responseBody.code, response.body);
            assert.equal(responseBody.error, "SyntaxError: Unexpected token '<'");
            assert.equal(responseBody.property, "body");

            assert.deepEqual(responseBody.args.body, {});
            assert.deepEqual(responseBody.args.path, path);
            assert.deepEqual(responseBody.args.query, {});
            assert.isNotNull(responseBody.args.headers);
          });
      });
    });

    it("Should process text/plain request", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const response = new MockResponse(
        MockResponseType.custom,
        `return ${StatusCodes.OK.toString()}`,
        MimeTypes.text,
        "if (this.body.startsWith('FORCE')) { return 'ACCEPTED' } return 'ERROR'"
      );

      const mock = new Mock(project.id, null, HttpMethod.post, "/test", response);
      await databaseHelper.batch(mockMapping, toMockDb(mock));

      // when
      // then
      await serverFaker
        .post(`/mock/${project.name}/test`)
        .set({
          "content-type": "text/plain",
        })
        .send("FORCE_RESULT")
        .expect(StatusCodes.OK, "ACCEPTED");
    });

    it("Should process xml request", async () => {
      // given
      const project = projectFixture.build();
      await databaseHelper.batch(projectMapping, project);

      const response = new MockResponse(
        MockResponseType.custom,
        `return ${StatusCodes.OK.toString()}`,
        MimeTypes.xml,
        "if(this.body.action === 'get__version') { return '<?xml version=\"1.0\"?><envelope><version>1.0.0</version></envelope>'; } return '<empty></empty>';"
      );

      const mock = new Mock(project.id, null, HttpMethod.post, "/test", response);
      await databaseHelper.batch(mockMapping, toMockDb(mock));

      // when
      // then
      await serverFaker
        .post(`/mock/${project.name}/test`)
        .set({
          "content-type": "application/xml",
        })
        .send(`<?xml version="1.0"?><action>get__version</action>`)
        .expect(StatusCodes.OK, '<?xml version="1.0"?><envelope><version>1.0.0</version></envelope>');
    });
  });
});
