import { StatusCodes } from "http-status-codes";
import { serverFaker } from "@test/faker/server-fake";

describe("Integration: Authorization", () => {
  describe("Auth: JWT Token Validation", () => {
    const path = "/manage/resources";

    it("Should return unauthorized for a empty token request", async () => {
      // given
      const headers = {};
      // when
      // then
      await serverFaker.get(path).set(headers).expect(StatusCodes.UNAUTHORIZED);
    });
    it("Should return unauthorized for a invalid token request", async () => {
      // given
      const headers = { authorization: "Bearer fake" };
      // when
      // then
      await serverFaker.get(path).set(headers).expect(StatusCodes.UNAUTHORIZED);
    });
    it("Should return the endpoint information for a valid token", async () => {
      // given
      const headers = { authorization: "bearer fakeAccessToken" };
      // when
      // then
      await serverFaker.get(path).set(headers).expect(StatusCodes.OK);
    });
  });
});
