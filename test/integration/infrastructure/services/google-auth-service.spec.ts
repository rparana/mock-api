/* eslint-disable @typescript-eslint/naming-convention */
import { StatusCodes } from "http-status-codes";
import nock from "nock";
import { assert } from "chai";
import { lorem } from "faker";
import { GoogleAuthService } from "@infrastructure/services/auth/google-auth-service";
import { GoogleOAuthFake } from "@test/faker/google-service-fake";
import { EnvSettings } from "@infrastructure/env-settings";
import { loggerFaker } from "@test/faker/logger-fake";

describe("Integration: Service", () => {
  describe("GoogleAuth", () => {
    beforeEach(() => {
      nock.cleanAll();
    });

    it("Should generate new google authentication token", async () => {
      // given
      const code = lorem.text();
      const redirectUri = `https://${lorem.text()}.com.br`;
      const responseBody = {
        expires_in: 2000,
        id_token: "id_token",
        refresh_token: "refresh_token",
        token_type: "Bearer",
      };
      GoogleOAuthFake.token(code, redirectUri).reply(StatusCodes.OK, responseBody);
      const service = new GoogleAuthService(new EnvSettings(), loggerFaker);

      // when
      const result = await service.getAccessToken(code, redirectUri);

      // then
      assert.deepEqual(result.accessToken, responseBody.id_token);
      assert.deepEqual(result.refreshToken, responseBody.refresh_token);
      assert.deepEqual(result.tokenType, responseBody.token_type);
      assert.deepEqual(result.expiresIn, responseBody.expires_in);
    });

    it("Should return empty token in case google return bad request", async () => {
      // given
      const code = lorem.text();
      const redirectUri = `https://${lorem.text()}.com.br`;
      GoogleOAuthFake.token(code, redirectUri).reply(StatusCodes.BAD_REQUEST);
      const service = new GoogleAuthService(new EnvSettings(), loggerFaker);

      // when
      const result = await service.getAccessToken(code, redirectUri);

      // then
      assert.deepEqual(result.accessToken, "");
      assert.deepEqual(result.refreshToken, "");
      assert.deepEqual(result.tokenType, "Bearer");
      assert.deepEqual(result.expiresIn, 0);
    });

    it("Should return empty token in case google return unauthorized", async () => {
      // given
      const code = lorem.text();
      const redirectUri = `https://${lorem.text()}.com.br`;
      GoogleOAuthFake.token(code, redirectUri).reply(StatusCodes.UNAUTHORIZED);
      const service = new GoogleAuthService(new EnvSettings(), loggerFaker);

      // when
      const result = await service.getAccessToken(code, redirectUri);

      // then
      assert.deepEqual(result.accessToken, "");
      assert.deepEqual(result.refreshToken, "");
      assert.deepEqual(result.tokenType, "Bearer");
      assert.deepEqual(result.expiresIn, 0);
    });

    it("Should return empty token in case code is empty", async () => {
      // given
      const code = "";
      const redirectUri = `https://${lorem.text()}.com.br`;
      GoogleOAuthFake.token(code, redirectUri).reply(StatusCodes.BAD_REQUEST);
      const service = new GoogleAuthService(new EnvSettings(), loggerFaker);

      // when
      const result = await service.getAccessToken(code, redirectUri);

      // then
      assert.deepEqual(result.accessToken, "");
      assert.deepEqual(result.refreshToken, "");
      assert.deepEqual(result.tokenType, "Bearer");
      assert.deepEqual(result.expiresIn, 0);
    });

    it("Should return empty token in case redirect Uri is empty", async () => {
      // given
      const code = lorem.text();
      const redirectUri = "";
      GoogleOAuthFake.token(code, redirectUri).reply(StatusCodes.BAD_REQUEST);
      const service = new GoogleAuthService(new EnvSettings(), loggerFaker);

      // when
      const result = await service.getAccessToken(code, redirectUri);

      // then
      assert.deepEqual(result.accessToken, "");
      assert.deepEqual(result.refreshToken, "");
      assert.deepEqual(result.tokenType, "Bearer");
      assert.deepEqual(result.expiresIn, 0);
    });
  });
});
