/* eslint-disable sonarjs/no-identical-functions */
import { assert, expect } from "chai";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { databaseHelper } from "@test/helpers/database-helper";
import { projectMapping } from "@infrastructure/data/repositories/postgres/mappings/project-mapping";
import { diTests } from "@test/di-tests";
import { UnitOfWork } from "@domain/contracts/repositories/unit-of-work";
import { projectFixture } from "@test/fixtures/project-fixture";
import { folderFixture } from "@test/fixtures/folder-fixture";

describe("Integration: Infrastructure", () => {
  describe("Repositories: Postgres", () => {
    beforeEach(async () => {
      await databaseHelper.clear(projectMapping);
    });

    describe("PostgresUnitOfWork", () => {
      it("Should execute a successful transaction", async () => {
        // given
        const unitOfWork = diTests.get(UnitOfWork);

        const project = projectFixture.build();
        const folder = folderFixture.withProjectId(project.id).build();

        // when
        await unitOfWork.transaction(async (repos) => {
          const { projectRepository, folderRepository } = repos;

          await projectRepository.createProject(project);
          await folderRepository.createFolder(folder);
        });

        // then
        const projects = diTests.get(ProjectRepository);
        const newProject = await projects.findById(project.id);
        assert.deepEqual(newProject, project);

        const folders = diTests.get(FolderRepository);
        const newFolder = await folders.findById(folder.id);
        assert.deepEqual(newFolder, folder);
      });

      it("Should execute a error transaction", async () => {
        // given
        const unitOfWork = diTests.get(UnitOfWork);

        const project = projectFixture.build();
        const folder = folderFixture.build();

        // when
        const run = async (): Promise<void> => {
          await unitOfWork.transaction(async (repos) => {
            const { projectRepository, folderRepository } = repos;

            await projectRepository.createProject(project);
            await folderRepository.createFolder(folder);
          });
        };

        await expect(run()).to.be.rejected;

        // then
        const projects = diTests.get(ProjectRepository);
        const newProject = await projects.findById(project.id);
        assert.isNull(newProject);

        const folders = diTests.get(FolderRepository);
        const newFolder = await folders.findById(folder.id);
        assert.isNull(newFolder);
      });
    });
  });
});
