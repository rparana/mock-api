/* eslint-disable @typescript-eslint/naming-convention */
import type { Interceptor } from "nock";
import nock from "nock";
import { EnvSettings } from "@infrastructure/env-settings";

export class GoogleOAuthFake {
  public static token(code: string, redirectUri: string): Interceptor {
    const settings = new EnvSettings();
    return nock(settings.googleOauth2BaseUrl).post("/token", {
      client_id: settings.googleClientId,
      client_secret: settings.googleSecretId,
      code,
      grant_type: "authorization_code",
      redirect_uri: redirectUri,
    });
  }
}
