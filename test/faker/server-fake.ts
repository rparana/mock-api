import { default as supertest } from "supertest";
import { Server } from "@interfaces/http/server";

const serverFaker = supertest(Server.run());
export { serverFaker };
