import type { Logger } from "@vizir/simple-json-logger";
import { spy } from "sinon";

export const loggerFaker = {
  debug: spy(),
  error: spy(),
  info: spy(),
  log: spy(),
  warn: spy(),
} as unknown as Logger;
