import { createContainer } from "@cross-cutting/di-container";

const testDIContainer = createContainer();
export { testDIContainer as diTests };
