/* eslint-disable @typescript-eslint/no-type-alias */
type Writeable<T> = { -readonly [P in keyof T]-?: T[P] };
type Modifier<T> = (attribures: Writeable<T>) => void;

export { Modifier, Writeable };
