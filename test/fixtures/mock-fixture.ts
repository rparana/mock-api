import { datatype, name as fakerName } from "faker";
import { Mock } from "@domain/entities/mock";
import { HttpMethod } from "@domain/enums/http-methods";
import type { Modifier, Writeable } from "./types";
import { mockResponseFixture } from "./mock-response-fixture";

class MockFixture {
  public modifiers: Modifier<Mock>[] = [];

  public build(): Mock {
    const response = mockResponseFixture.buildSimple();

    const mock = new Mock(datatype.uuid(), null, HttpMethod.get, `/${fakerName.firstName()}`, response);

    this.modifiers.forEach((modifier) => {
      modifier(mock as Writeable<Mock>);
    });

    return mock;
  }

  public withProjectId(projectId: string): MockFixture {
    return this.newFixture((attributes) => {
      Object.assign(attributes, {
        projectId,
      });
    });
  }

  public withFolderId(folderId: string): MockFixture {
    return this.newFixture((attributes) => {
      Object.assign(attributes, {
        folderId,
      });
    });
  }

  private newFixture(modifier: Modifier<Mock>): MockFixture {
    const newFixture = new MockFixture();
    newFixture.modifiers = [...this.modifiers, modifier];

    return newFixture;
  }
}

const mockFixture = new MockFixture();
export { MockFixture, mockFixture };
