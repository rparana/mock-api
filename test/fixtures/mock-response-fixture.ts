import { name as fakerName } from "faker";
import { StatusCodes } from "http-status-codes";
import { MockResponse } from "@domain/value-objects/mock-response";
import { MockResponseType } from "@domain/enums/mock-response-type";
import { MimeTypes } from "@domain/enums/mime-types";
import type { Modifier, Writeable } from "./types";

class MockResponseFixture {
  public modifiers: Modifier<MockResponse>[] = [];

  public buildSimple(): MockResponse {
    const response = new MockResponse(MockResponseType.simple, StatusCodes.OK.toString(), MimeTypes.json, null, null);

    this.modifiers.forEach((modifier) => {
      modifier(response as Writeable<MockResponse>);
    });

    return response;
  }

  public buildCustom(): MockResponse {
    const response = new MockResponse(
      MockResponseType.custom,
      ` return ${StatusCodes.OK.toString()}`,
      MimeTypes.json,
      `return "${fakerName.firstName()}"`,
      `return ${JSON.stringify({ headerKey: fakerName.firstName() })}`
    );

    this.modifiers.forEach((modifier) => {
      modifier(response as Writeable<MockResponse>);
    });

    return response;
  }
}

const mockResponseFixture = new MockResponseFixture();
export { MockResponseFixture, mockResponseFixture };
