import { datatype, name as fakerName } from "faker";
import { times } from "lodash";
import { Folder } from "@domain/entities/folder";
import type { Modifier, Writeable } from "./types";

const TIMES = 5;

class FolderFixture {
  public modifiers: Modifier<Folder>[] = [];

  public build(): Folder {
    const folder = new Folder(datatype.uuid(), fakerName.firstName());

    this.modifiers.forEach((modifier) => {
      modifier(folder as Writeable<Folder>);
    });

    return folder;
  }

  public buildMany(count = TIMES): Folder[] {
    return times(count, () => this.build());
  }

  public withName(name: string): FolderFixture {
    return this.newFixture((attributes) => {
      Object.assign(attributes, {
        _name: name,
      });
    });
  }

  public withProjectId(projectId: string): FolderFixture {
    return this.newFixture((attributes) => {
      Object.assign(attributes, {
        projectId,
      });
    });
  }

  private newFixture(modifier: Modifier<Folder>): FolderFixture {
    const newFixture = new FolderFixture();
    newFixture.modifiers = [...this.modifiers, modifier];

    return newFixture;
  }
}

const folderFixture = new FolderFixture();
export { FolderFixture, folderFixture };
