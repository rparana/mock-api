import { name as fakerName } from "faker";
import { times } from "lodash";
import { Project } from "@domain/entities/project";
import type { Modifier, Writeable } from "./types";

const TIMES = 5;

class ProjectFixture {
  public modifiers: Modifier<Project>[] = [];

  public build(): Project {
    const project = new Project(fakerName.firstName().toLowerCase());

    this.modifiers.forEach((modifier) => {
      modifier(project as Writeable<Project>);
    });

    return project;
  }

  public buildMany(count = TIMES): Project[] {
    return times(count, () => this.build());
  }

  public withName(name: string): ProjectFixture {
    return this.newFixture((attributes) => {
      Object.assign(attributes, {
        _name: name,
      });
    });
  }

  private newFixture(modifier: Modifier<Project>): ProjectFixture {
    const newFixture = new ProjectFixture();
    newFixture.modifiers = [...this.modifiers, modifier];

    return newFixture;
  }
}

const projectFixture = new ProjectFixture();
export { ProjectFixture, projectFixture };
