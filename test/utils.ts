import { cloneDeepWith } from "lodash";

export const assignObject = <T>(value: T, toChange: Partial<T>): T => {
  const copy = cloneDeepWith(value);

  return Object.assign(copy, toChange);
};
