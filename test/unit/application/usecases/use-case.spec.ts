import { assert } from "chai";
import { Container } from "inversify";
import { Logger } from "@vizir/simple-json-logger";
import { Result } from "@application/entities/result";
import { ResultType } from "@application/enums/result-type";
import { UseCase } from "@application/use-cases/use-case";
import { loggerFaker } from "@test/faker/logger-fake";

class ErrorUseCase extends UseCase {
  protected async execute(): Promise<Result> {
    await Promise.reject();

    return Result.success();
  }
}

describe("Unit: Application", () => {
  describe("UseCase", () => {
    it("Should return error if internal error in usecase", async () => {
      // given
      const container = new Container();
      container.bind(Logger).toConstantValue(loggerFaker);
      container.bind(ErrorUseCase).toSelf();

      const useCase = container.get(ErrorUseCase);

      // when
      const result = await useCase.handler();

      // then
      assert.isNull(result.data);
      assert.isTrue(result.isEmpty);
      assert.equal(ResultType.error, result.type);
    });
  });
});
