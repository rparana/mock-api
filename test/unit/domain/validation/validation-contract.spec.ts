import { assert } from "chai";
import { name } from "faker";
import { ValidationContract } from "@domain/validation/validation-contract";

const PROPERTY = "property-name";
const VALIDATION_NAME = "validate-pattern";

class ValidationContractObject extends ValidationContract {
  private value!: string;

  public changeValueRegexPattern(regex: RegExp, newValue: string, message = ""): void {
    this.value = newValue;

    this.isInvalidPattern(regex, PROPERTY, this.value, VALIDATION_NAME, message);
  }

  public changeValueLenght(maxLenght: number, newValue: string): void {
    this.value = newValue;

    this.isLengthGreaterThan(maxLenght, PROPERTY, this.value);
  }
}

describe("Unit: Domain", () => {
  describe("Validation: ValidationContract", () => {
    describe("Lenght validation", () => {
      it("Should return error value lenght is grether than acceptable", () => {
        // given
        const validationContractObject = new ValidationContractObject();
        const value = name.firstName();
        const maxValueLenght = value.length - 1;

        // when
        validationContractObject.changeValueLenght(maxValueLenght, value);

        // then
        const message = `${PROPERTY} length must not be greater than ${maxValueLenght}`;

        assert.isTrue(validationContractObject.isInvalid());
        assert.strictEqual(validationContractObject.errors[0].value, value);
        assert.strictEqual(validationContractObject.errors[0].property, PROPERTY);
        assert.deepEqual(validationContractObject.errors[0].validation, { isLengthGreaterThan: `${message}` });
      });

      it("Should return no errors for value lenght equal to acceptable", () => {
        // given
        const validationContractObject = new ValidationContractObject();
        const value = name.firstName();
        const maxValueLenght = value.length;

        // when
        validationContractObject.changeValueLenght(maxValueLenght, value);

        // then
        assert.isFalse(validationContractObject.isInvalid());
        assert.lengthOf(validationContractObject.errors, 0);
      });
    });

    describe("Regex Pattern validation", () => {
      it("Should return validation error when value does not match the regex", () => {
        // given
        const validationContractObject = new ValidationContractObject();
        const value = name.firstName();
        const regexPattern = /[^0-9]/gm;
        const message = `${name.firstName()} ${name.lastName()}`;

        // when
        validationContractObject.changeValueRegexPattern(regexPattern, value, message);
        // then
        assert.isTrue(validationContractObject.isInvalid());
        assert.strictEqual(validationContractObject.errors[0].value, value);
        assert.strictEqual(validationContractObject.errors[0].property, PROPERTY);
        assert.deepEqual(validationContractObject.errors[0].validation, { [VALIDATION_NAME]: message });
      });

      it("Should return no errors when value matches the regex", () => {
        // given
        const validationContractObject = new ValidationContractObject();
        const value = name.firstName();
        const regexPattern = RegExp(`/${value}/gm`);

        // when
        validationContractObject.changeValueRegexPattern(regexPattern, value);
        // then
        assert.isFalse(validationContractObject.isInvalid());
        assert.lengthOf(validationContractObject.errors, 0);
      });
    });
  });
});
