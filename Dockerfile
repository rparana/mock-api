FROM node:12.14.1-alpine as builder

WORKDIR /app
COPY . /app

RUN npm i
RUN npm run generate:docs
RUN npm run build
RUN npm prune --production
RUN cp -r node_modules dist
RUN cp package.json dist
RUN cp mock-api-swagger.json dist

FROM node:12.14.1-alpine
WORKDIR /app
COPY --from=builder /app/dist .
EXPOSE 3000
RUN rm -rf test
RUN ls
CMD NODE_URLS=http://*:$PORT npm start
