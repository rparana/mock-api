up:
	docker-compose up -d

down:
	docker-compose down

console:
	docker-compose exec api sh

logs:
	docker-compose logs -f --tail=1000 api
