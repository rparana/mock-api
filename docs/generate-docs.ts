import { version } from "../package.json";
import { writeFileSync } from "fs";
import YAML from "js-yaml";
import { OpenAPI } from "openapi-types";
import { default as Swagger } from "swagger-parser";

const JSON_SPACE = 4;

const generate = (api: OpenAPI.Document): void => {
  const doc = YAML.dump({
    ...api,
    info: { ...api.info, version },
    servers: [
      { url: "http://localhost:3000", description: "Local" },
      { url: "https://vizir-mock-api.herokuapp.com", description: "Prod" },
    ],
  });

  const json = JSON.stringify(YAML.load(doc), null, JSON_SPACE);

  writeFileSync("./mock-api-swagger.json", json, "utf-8");
  writeFileSync("./mock-api-swagger.yml", doc, "utf-8");
};

// tslint:disable-next-line: no-floating-promises
Swagger.dereference("./docs/base.swagger.yml").then(generate);
