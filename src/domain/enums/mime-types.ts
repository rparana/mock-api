export enum MimeTypes {
  text = "text/plain",
  xml = "application/xml",
  json = "application/json",
}
