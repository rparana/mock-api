export enum HttpMethod {
  get = "GET",
  head = "HEAD",
  post = "POST",
  put = "PUT",
  delete = "DELETE",
  options = "OPTIONS",
  patch = "PATCH",
}
