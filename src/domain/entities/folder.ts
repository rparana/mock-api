import { v4 } from "uuid";
import { ValidationContract } from "@domain/validation/validation-contract";

const MAX_FOLDER_LENGTH = 40;

export class Folder extends ValidationContract {
  public readonly id: string;

  public readonly projectId: string;

  private _name: string;

  public get name(): string {
    return this._name;
  }

  public constructor(projectId: string, name: string) {
    super();
    this.id = v4();
    this.projectId = projectId;
    this._name = "";

    this.changeName(name);
  }

  public changeName(newName: string): void {
    this._name = newName;

    this.isLengthGreaterThan(MAX_FOLDER_LENGTH, "name", this.name);
  }
}
