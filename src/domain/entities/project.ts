import { v4 } from "uuid";
import { ValidationContract } from "@domain/validation/validation-contract";

const projectNamePattern = /[^a-z_0-9]/gm;
const MAX_PROJECT_LENGTH = 40;

export class Project extends ValidationContract {
  public readonly id: string;

  private _name: string;

  public get name(): string {
    return this._name;
  }

  public constructor(name: string) {
    super();
    this.id = v4();
    this._name = "";

    this.changeName(name);
  }

  public changeName(newName: string): void {
    this._name = newName;

    this.isInvalidPattern(
      projectNamePattern,
      "name",
      this._name,
      "projectName",
      "name accepts lowercase letters, numbers or _ (underline)"
    );

    this.isLengthGreaterThan(MAX_PROJECT_LENGTH, "name", this.name);
  }
}
