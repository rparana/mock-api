import { v4 } from "uuid";
import type { MatchResult } from "path-to-regexp";
import { match } from "path-to-regexp";
import { isEmpty } from "lodash";
import type { HttpMethod } from "@domain/enums/http-methods";
import type { MockResponse } from "@domain/value-objects/mock-response";
import type { Dictionary, Nullable } from "@utils/types";

export class Mock {
  public readonly id: string;

  public readonly projectId: string;

  public readonly folderId: Nullable<string>;

  public get active(): boolean {
    return this._active;
  }

  private _active: boolean;

  public get method(): HttpMethod {
    return this._method;
  }

  private _method: HttpMethod;

  public get path(): string {
    return this._path;
  }

  private _path: string;

  public get response(): MockResponse {
    return this._response;
  }

  private _response: MockResponse;

  public constructor(
    projectId: string,
    folderId: Nullable<string>,
    method: HttpMethod,
    path: string,
    response: MockResponse
  ) {
    this.id = v4();
    this._active = true;
    this.projectId = projectId;
    this.folderId = folderId;
    this._method = method;
    this._path = path;
    this._response = response;
  }

  public match(urlToCompare: string): boolean {
    return match(this.path)(urlToCompare) !== false;
  }

  public extractPathVariables(urlToExtract: string): Nullable<Dictionary> {
    if (!this.match(urlToExtract)) {
      return null;
    }

    const { params } = match(this.path)(urlToExtract) as MatchResult<Dictionary>;
    return isEmpty(params) ? null : params;
  }

  public editMock(method: HttpMethod, path: string, response: MockResponse): void {
    this._method = method;
    this._path = path;
    this._response = response;
  }

  public changeStatus(active: boolean): void {
    this._active = active;
  }
}
