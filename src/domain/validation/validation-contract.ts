import { ValidationError } from "./validation-error";

export abstract class ValidationContract {
  private readonly _errors: ValidationError[];

  public constructor() {
    this._errors = [];
  }

  public isInvalid(): boolean {
    return this._errors.length > 0;
  }

  public get errors(): Readonly<ValidationError[]> {
    return this._errors;
  }

  protected isLengthGreaterThan(max: number, property: string, value: string): void {
    if (value.length > max) {
      this.addError(property, value, "isLengthGreaterThan", `${property} length must not be greater than ${max}`);
    }
  }

  protected isInvalidPattern(
    regExp: RegExp,
    property: string,
    value: string,
    validationName: string,
    message: string
  ): void {
    if (!regExp.test(value)) {
      return;
    }

    this.addError(property, value, validationName, message);
  }

  private addError(property: string, value: unknown, validationName: string, message: string): void {
    this._errors.push(
      new ValidationError(property, value, {
        [validationName]: message,
      })
    );
  }
}
