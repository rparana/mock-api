export class ValidationError {
  public readonly property: string;

  public readonly value?: unknown;

  public readonly validation?: Record<string, string>;

  public readonly children?: ValidationError[];

  public constructor(
    property: string,
    value?: unknown,
    validation?: Record<string, string>,
    children?: ValidationError[]
  ) {
    this.value = value;
    this.property = property;
    this.children = children;
    this.validation = validation;
  }
}
