import type { ProjectRepository } from "./project-repository";
import type { FolderRepository } from "./folder-repository";

export interface Repositories {
  projectRepository: ProjectRepository;
  folderRepository: FolderRepository;
}

export abstract class UnitOfWork {
  public abstract transaction(callback: (repositories: Repositories) => Promise<void>): Promise<void>;
}
