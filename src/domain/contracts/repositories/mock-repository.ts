import type { Mock } from "@domain/entities/mock";
import type { HttpMethod } from "@domain/enums/http-methods";

export abstract class MockRepository {
  abstract getActives(projectId: string, method: HttpMethod): Promise<Mock[]>;

  abstract createMock(mock: Mock): Promise<void>;

  abstract deleteMock(projectId: string, mockId: string): Promise<void>;

  abstract editMock(mock: Mock): Promise<void>;

  abstract findAllInProject(projectId: string): Promise<Mock[]>;

  abstract findById(id: string): Promise<Mock | null>;

  abstract getMock(projectId: string, method: HttpMethod, path: string): Promise<Mock | null>;
}
