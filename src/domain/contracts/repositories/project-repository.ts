import type { Project } from "@domain/entities/project";

export abstract class ProjectRepository {
  abstract find: (projectName: string) => Promise<Project | null>;

  abstract findById: (projectId: string) => Promise<Project | null>;

  abstract createProject: (project: Project) => Promise<void>;

  abstract editProject: (project: Project) => Promise<void>;

  abstract deleteProject: (projectId: string) => Promise<void>;

  abstract findAllProjects: () => Promise<Project[]>;
}
