import type { Folder } from "@domain/entities/folder";

export abstract class FolderRepository {
  abstract find: (projectId: string, folder: string) => Promise<Folder | null>;

  abstract findById: (folderId: string) => Promise<Folder | null>;

  abstract createFolder: (folder: Folder) => Promise<void>;

  abstract editFolder: (folder: Folder) => Promise<void>;

  abstract deleteFolder: (projectId: string, folderId: string) => Promise<void>;

  abstract deleteProjectFolders: (projectId: string) => Promise<void>;

  abstract findAllInProject: (projectId: string) => Promise<Folder[]>;
}
