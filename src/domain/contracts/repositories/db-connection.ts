export abstract class DbConnection {
  public abstract close(): Promise<void>;
}
