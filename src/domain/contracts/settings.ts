export abstract class Settings {
  public abstract get dbName(): string;

  public abstract get dbHost(): string;

  public abstract get dbPassword(): string;

  public abstract get dbPort(): number;

  public abstract get dbUser(): string;

  public abstract get dbSsl(): boolean;

  public abstract get enabledSwagger(): boolean;

  public abstract get mockAuth(): boolean;

  public abstract get googleOauth2BaseUrl(): string;

  public abstract get googleClientId(): string;

  public abstract get googleSecretId(): string;
}
