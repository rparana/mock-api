import type { TokenDto } from "./token-dto";

export abstract class AuthService {
  abstract getAccessToken: (code: string, redirectUri: string) => Promise<TokenDto>;

  abstract isValid: (accessToken: string) => Promise<boolean>;
}
