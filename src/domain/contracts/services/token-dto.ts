import { TokenTypes } from "@domain/enums/token-types";

export class TokenDto {
  public readonly accessToken: string;

  public readonly refreshToken: string;

  public readonly tokenType: TokenTypes;

  public readonly expiresIn: number;

  public constructor(accessToken: string, refreshToken: string, tokenType: TokenTypes, expiresIn: number) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.tokenType = tokenType;
    this.expiresIn = expiresIn;
  }

  public static empty(): TokenDto {
    return new TokenDto("", "", TokenTypes.bearer, 0);
  }

  public isEmpty(): boolean {
    return this.accessToken === "";
  }
}
