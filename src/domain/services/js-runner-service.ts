/* eslint-disable @typescript-eslint/no-implied-eval */
import { isNullOrUndefined } from "util";
import { isNull } from "lodash";
import { injectable } from "inversify";
import type { CustomResponseArgs, MockResponse } from "@domain/value-objects/mock-response";
import type { Dictionary, Nullable } from "@utils/types";
import type { ExecutionResult } from "@domain/value-objects/execution-result";
import { ExecutionError, ExecutionSuccess } from "@domain/value-objects/execution-result";

@injectable()
export class JsRunnerService {
  public execute(response: MockResponse, args: CustomResponseArgs): ExecutionResult {
    let currentCode = "";
    let currentProp = "";

    const processJS = <T>(prop: string, code: Nullable<string>): Nullable<T> | undefined => {
      currentProp = prop;

      if (isNull(code)) {
        return null;
      }

      currentCode = code;

      return new Function(code).call(args) as T;
    };

    try {
      const status = processJS<number>("status", response.status);

      if (isNullOrUndefined(status)) {
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        return new ExecutionError(currentCode, currentProp, args, `Code dont return a valid http status: ${status}`);
      }

      const body = processJS<Dictionary>("body", response.body) ?? null;
      const headers = processJS<Dictionary>("headers", response.headers) ?? null;

      return new ExecutionSuccess(status, response.mimeType, body, headers);
    } catch (ex) {
      return new ExecutionError(currentCode, currentProp, args, ex);
    }
  }
}
