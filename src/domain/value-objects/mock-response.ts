import type { MimeTypes } from "@domain/enums/mime-types";
import type { Dictionary, Nullable } from "@utils/types";
import { MockResponseType } from "@domain/enums/mock-response-type";

export interface CustomResponseArgs {
  headers: Nullable<Dictionary>;
  body: Nullable<unknown>;
  query: Nullable<Dictionary>;
  path: Nullable<Dictionary>;
}

export class MockResponse {
  public readonly status: string;

  public readonly body: Nullable<string>;

  public readonly mimeType: MimeTypes;

  public readonly headers: Nullable<string>;

  public readonly type: MockResponseType;

  public constructor(
    type: MockResponseType,
    status: string,
    mimeType: MimeTypes,
    body: Nullable<string> = null,
    headers: Nullable<string> = null
  ) {
    this.type = type;
    this.status = status;
    this.body = body;
    this.mimeType = mimeType;
    this.headers = headers;
  }

  public isSimple(): boolean {
    return this.type === MockResponseType.simple;
  }
}
