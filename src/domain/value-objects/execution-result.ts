import type { MimeTypes } from "@domain/enums/mime-types";
import type { Dictionary, Nullable } from "@utils/types";
import type { CustomResponseArgs } from "./mock-response";

export interface ExecutionResult {
  success: boolean;
}

export class ExecutionError implements ExecutionResult {
  public success = false;

  public readonly code: string;

  public readonly property: string;

  public readonly args: CustomResponseArgs;

  public readonly error: string;

  public constructor(code: string, property: string, args: CustomResponseArgs, error: unknown) {
    this.code = code;
    this.property = property;
    this.args = args;
    // eslint-disable-next-line @typescript-eslint/no-base-to-string
    this.error = (error as Error).toString();
  }
}

export class ExecutionSuccess implements ExecutionResult {
  public success = true;

  public readonly status: number;

  public readonly mimeType: MimeTypes;

  public readonly body: Nullable<unknown>;

  public readonly headers: Nullable<Dictionary>;

  public constructor(status: number, mimeType: MimeTypes, body: Nullable<unknown>, headers: Nullable<Dictionary>) {
    this.mimeType = mimeType;
    this.status = status;
    this.body = body;
    this.headers = headers;
  }
}
