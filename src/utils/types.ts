/* eslint-disable @typescript-eslint/no-type-alias */
import type { Request } from "express";
import type { Container } from "inversify";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Constructor<T> = new (...args: any[]) => T;

export type Nullable<T> = T | null;

export interface RequestContainer extends Request {
  container: Container;
}

export type Dictionary = Record<string, unknown>;

// eslint-disable-next-line @typescript-eslint/ban-types
type PropertiesName<T> = { [K in keyof T]: T[K] extends Function ? never : K }[keyof T];
export type PlainObject<T> = Pick<T, PropertiesName<T>>;
