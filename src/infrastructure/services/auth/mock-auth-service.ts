import { injectable } from "inversify";
import type { AuthService } from "@domain/contracts/services/auth-service";
import { TokenDto } from "@domain/contracts/services/token-dto";
import { TokenTypes } from "@domain/enums/token-types";

const ACCESS_TOKEN = "fakeAccessToken";

@injectable()
export class MockAuthService implements AuthService {
  public async getAccessToken(): Promise<TokenDto> {
    const TOKEN_EXPIRATION = 3600;

    const token = new TokenDto(ACCESS_TOKEN, "refreshToken", TokenTypes.bearer, TOKEN_EXPIRATION);

    return Promise.resolve(token);
  }

  public async isValid(accessToken: string): Promise<boolean> {
    const result = accessToken === ACCESS_TOKEN;

    return Promise.resolve(result);
  }
}
