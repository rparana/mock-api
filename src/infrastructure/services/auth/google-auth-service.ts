/* eslint-disable @typescript-eslint/naming-convention */
import { inject, injectable } from "inversify";
import { Logger } from "@vizir/simple-json-logger";
import type { AxiosInstance, AxiosResponse } from "axios";
import { TokenDto } from "@domain/contracts/services/token-dto";
import type { TokenTypes } from "@domain/enums/token-types";
import { Settings } from "@domain/contracts/settings";
import type { AuthService } from "@domain/contracts/services/auth-service";
import { HttpClient } from "../http-client";

interface GoogleTokenResponse {
  id_token: string;
  access_token: string;
  refresh_token: string;
  token_type: TokenTypes;
  expires_in: number;
}

@injectable()
export class GoogleAuthService implements AuthService {
  private readonly settings: Settings;

  private readonly logger: Logger;

  private readonly client: AxiosInstance;

  public constructor(@inject(Settings) settings: Settings, @inject(Logger) logger: Logger) {
    this.settings = settings;
    this.logger = logger;
    this.client = HttpClient.create(this.settings.googleOauth2BaseUrl);
  }

  public async getAccessToken(code: string, redirectUri: string): Promise<TokenDto> {
    try {
      const res: AxiosResponse<GoogleTokenResponse> = await this.client.post("/token", {
        client_id: this.settings.googleClientId,
        client_secret: this.settings.googleSecretId,
        code,
        grant_type: "authorization_code",
        redirect_uri: redirectUri,
      });

      return new TokenDto(
        res.data.id_token,
        res.data.refresh_token,
        res.data.token_type as TokenTypes,
        res.data.expires_in
      );
    } catch (err) {
      this.logger.error("Could not generate token", { err });
      return TokenDto.empty();
    }
  }

  public async isValid(accessToken: string): Promise<boolean> {
    try {
      await this.client.post("/tokeninfo", {
        id_token: accessToken,
      });
      return true;
    } catch (err) {
      this.logger.error("Could not validate token", { err });
      return false;
    }
  }
}
