import type { Settings } from "@domain/contracts/settings";

export class EnvSettings implements Settings {
  public get dbSsl(): boolean {
    return this.getBooleanValue("DB_SSL");
  }

  public get dbName(): string {
    return this.getValueOrDefault("DB_NAME");
  }

  public get dbHost(): string {
    return this.getValueOrDefault("DB_HOST");
  }

  public get dbPassword(): string {
    return this.getValueOrDefault("DB_PASSWORD");
  }

  public get dbPort(): number {
    const port = this.getValueOrDefault("DB_PORT", "0");

    return parseInt(port);
  }

  public get dbUser(): string {
    return this.getValueOrDefault("DB_USER");
  }

  public get enabledSwagger(): boolean {
    return this.getBooleanValue("ENABLED_SWAGGER");
  }

  public get mockAuth(): boolean {
    return this.getBooleanValue("MOCK_AUTH");
  }

  public get googleOauth2BaseUrl(): string {
    return this.getValueOrDefault("GOOGLE_OAUTH2_BASE_URL");
  }

  public get googleClientId(): string {
    return this.getValueOrDefault("GOOGLE_CLIENT_ID");
  }

  public get googleSecretId(): string {
    return this.getValueOrDefault("GOOGLE_SECRET_ID");
  }

  private getValueOrDefault(key: string, defaultValue = ""): string {
    return process.env[key] ?? defaultValue;
  }

  private getBooleanValue(name: string): boolean {
    return Boolean(Number(this.getValueOrDefault(name, "0")));
  }
}
