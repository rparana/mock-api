import type { MigrationInterface, QueryRunner } from "typeorm";
import { TableForeignKey, Table } from "typeorm";

const TABLE_NAME = "folders";

export class CreateFolderTable20211021114211 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isPrimary: true,
            name: "id",
            type: "uuid",
          },
          {
            length: "40",
            name: "name",
            type: "varchar",
          },
          {
            name: "project_id",
            type: "uuid",
          },
        ],
        name: TABLE_NAME,
      })
    );
    await queryRunner.createForeignKey(
      TABLE_NAME,
      new TableForeignKey({
        columnNames: ["project_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "projects",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_NAME);
  }
}
