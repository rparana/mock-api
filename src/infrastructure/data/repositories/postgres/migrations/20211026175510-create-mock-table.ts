import type { MigrationInterface, QueryRunner } from "typeorm";
import { Table, TableForeignKey } from "typeorm";

const TABLE_NAME = "mocks";

export class CreateMockTable20211026175510 implements MigrationInterface {
  async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isPrimary: true,
            name: "id",
            type: "uuid",
          },
          {
            name: "project_id",
            type: "uuid",
          },
          {
            isNullable: true,
            name: "folder_id",
            type: "uuid",
          },
          {
            enum: ["GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"],
            name: "method",
            type: "enum",
          },
          {
            length: "150",
            name: "path",
            type: "varchar",
          },
          {
            enum: ["simple", "custom"],
            name: "response_type",
            type: "enum",
          },
          {
            default: true,
            name: "active",
            type: "boolean",
          },
          {
            name: "status",
            type: "varchar",
          },
          {
            isNullable: true,
            name: "body",
            type: "varchar",
          },
          {
            isNullable: true,
            name: "headers",
            type: "varchar",
          },
          {
            enum: ["text/plain", "application/xml", "application/json"],
            name: "mime_type",
            type: "enum",
          },
        ],
        name: TABLE_NAME,
      })
    );

    await queryRunner.createForeignKey(
      TABLE_NAME,
      new TableForeignKey({
        columnNames: ["project_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "projects",
      })
    );

    await queryRunner.createForeignKey(
      TABLE_NAME,
      new TableForeignKey({
        columnNames: ["folder_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "folders",
      })
    );
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_NAME);
  }
}
