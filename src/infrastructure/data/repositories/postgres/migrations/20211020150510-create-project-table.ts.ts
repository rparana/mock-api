import type { MigrationInterface, QueryRunner } from "typeorm";
import { Table } from "typeorm";

const TABLE_NAME = "projects";

export class CreateProjectTable20211020150510 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        columns: [
          {
            isPrimary: true,
            name: "id",
            type: "uuid",
          },
          {
            isUnique: true,
            length: "40",
            name: "name",
            type: "varchar",
          },
        ],
        name: TABLE_NAME,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TABLE_NAME);
  }
}
