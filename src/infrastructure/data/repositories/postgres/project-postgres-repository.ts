import { inject, injectable } from "inversify";
import type { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import type { Project } from "@domain/entities/project";
import { DbConnection } from "@domain/contracts/repositories/db-connection";
import { PostgresDbConnection } from "./postgres-db-connection";
import { projectMapping, toProjectEntity } from "./mappings/project-mapping";

@injectable()
export class ProjectPostgresRepository implements ProjectRepository {
  private readonly connection: PostgresDbConnection;

  public constructor(@inject(DbConnection) connection: PostgresDbConnection) {
    this.connection = connection;
  }

  public async find(projectName: string): Promise<Project | null> {
    const repo = await this.connection.getRepository(projectMapping);

    const result = await repo.findOne({ name: projectName });

    return toProjectEntity(result);
  }

  public async findById(projectId: string): Promise<Project | null> {
    const repo = await this.connection.getRepository(projectMapping);

    const result = await repo.findOne(projectId);

    return toProjectEntity(result);
  }

  public async createProject(project: Project): Promise<void> {
    const repo = await this.connection.getRepository(projectMapping);

    await repo.save(project);
  }

  public async editProject(project: Project): Promise<void> {
    await this.createProject(project);
  }

  public async deleteProject(projectId: string): Promise<void> {
    const repo = await this.connection.getRepository(projectMapping);
    await repo.delete(projectId);
  }

  public async findAllProjects(): Promise<Project[]> {
    const repo = await this.connection.getRepository(projectMapping);

    const result = await repo.find({
      order: {
        name: "ASC",
      },
    });

    return result.map((item) => toProjectEntity(item)) as Project[];
  }
}
