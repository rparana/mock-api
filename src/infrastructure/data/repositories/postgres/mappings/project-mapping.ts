import { isNullOrUndefined } from "util";
import { EntitySchema } from "typeorm";
import { Project } from "@domain/entities/project";

export const projectMapping: EntitySchema<Project> = new EntitySchema<Project>({
  columns: {
    id: {
      primary: true,
      type: "uuid",
      unique: true,
    },
    name: {
      length: 40,
      nullable: false,
      type: String,
      unique: true,
    },
  },
  name: "projects",
});

export const toProjectEntity = (data: Project | undefined): Project | null => {
  if (isNullOrUndefined(data)) {
    return null;
  }

  const project = new Project(data.name);
  Object.assign(project, {
    id: data.id,
  });

  return project;
};
