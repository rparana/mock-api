import { isNullOrUndefined } from "util";
import { EntitySchema } from "typeorm";
import { Folder } from "@domain/entities/folder";

export const folderMapping: EntitySchema<Folder> = new EntitySchema<Folder>({
  columns: {
    id: {
      primary: true,
      type: "uuid",
      unique: true,
    },
    name: {
      length: 40,
      nullable: false,
      type: String,
    },
    projectId: {
      name: "project_id",
      nullable: false,
      type: "uuid",
    },
  },
  name: "folders",
});

export const toFolderEntity = (data: Folder | undefined): Folder | null => {
  if (isNullOrUndefined(data)) {
    return null;
  }

  const folder = new Folder(data.projectId, data.name);
  Object.assign(folder, {
    id: data.id,
  });

  return folder;
};
