/* eslint-disable sort-keys */
import { isNullOrUndefined } from "util";
import { EntitySchema } from "typeorm";
import { Mock } from "@domain/entities/mock";
import { HttpMethod } from "@domain/enums/http-methods";
import { MockResponseType } from "@domain/enums/mock-response-type";
import { MockResponse } from "@domain/value-objects/mock-response";
import { MimeTypes } from "@domain/enums/mime-types";
import type { Nullable } from "@utils/types";

interface MockDb {
  id: string;
  projectId: string;
  folderId: Nullable<string>;
  method: HttpMethod;
  path: string;
  active: boolean;
  status: string;
  body: Nullable<string>;
  mimeType: MimeTypes;
  headers: Nullable<string>;
  type: MockResponseType;
}

export const mockMapping: EntitySchema<MockDb> = new EntitySchema<MockDb>({
  columns: {
    id: {
      primary: true,
      type: "uuid",
      unique: true,
    },
    projectId: {
      name: "project_id",
      type: "uuid",
    },
    folderId: {
      name: "folder_id",
      nullable: true,
      type: "uuid",
    },
    method: {
      name: "method",
      type: String,
      enum: HttpMethod,
    },
    path: {
      name: "path",
      type: String,
    },
    active: {
      name: "active",
      type: Boolean,
    },
    type: {
      name: "response_type",
      type: String,
      enum: MockResponseType,
    },
    status: {
      name: "status",
      type: String,
    },
    mimeType: {
      name: "mime_type",
      type: String,
      enum: MimeTypes,
    },
    headers: {
      name: "headers",
      type: String,
      nullable: true,
    },
    body: {
      name: "body",
      type: String,
      nullable: true,
    },
  },
  name: "mocks",
});

export const toMockDb = (data: Mock): MockDb => {
  return {
    active: data.active,
    body: data.response.body,
    folderId: data.folderId,
    headers: data.response.headers,
    id: data.id,
    method: data.method,
    mimeType: data.response.mimeType,
    path: data.path,
    projectId: data.projectId,
    status: data.response.status,
    type: data.response.type,
  };
};

export const toMockEntity = (data: MockDb | undefined): Mock | null => {
  if (isNullOrUndefined(data)) {
    return null;
  }

  const response = new MockResponse(data.type, data.status, data.mimeType, data.body, data.headers);

  const mock = new Mock(data.projectId, data.folderId, data.method, data.path, response);

  Object.assign(mock, {
    id: data.id,
    _active: data.active,
  });

  return mock;
};
