import { inject, injectable } from "inversify";
import type { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { DbConnection } from "@domain/contracts/repositories/db-connection";
import type { Folder } from "@domain/entities/folder";
import { PostgresDbConnection } from "./postgres-db-connection";
import { folderMapping, toFolderEntity } from "./mappings/folder-mapping";

@injectable()
export class FolderPostgresRepository implements FolderRepository {
  private readonly connection: PostgresDbConnection;

  public constructor(@inject(DbConnection) connection: PostgresDbConnection) {
    this.connection = connection;
  }

  public async find(projectId: string, folder: string): Promise<Folder | null> {
    const repo = await this.connection.getRepository(folderMapping);

    const result = await repo.findOne({
      where: {
        name: folder,
        projectId,
      },
    });

    return toFolderEntity(result);
  }

  public async findById(folderId: string): Promise<Folder | null> {
    const repo = await this.connection.getRepository(folderMapping);

    const result = await repo.findOne(folderId);

    return toFolderEntity(result);
  }

  public async createFolder(folder: Folder): Promise<void> {
    const repo = await this.connection.getRepository(folderMapping);

    await repo.save(folder);
  }

  public async editFolder(folder: Folder): Promise<void> {
    await this.createFolder(folder);
  }

  public async deleteFolder(projectId: string, folderId: string): Promise<void> {
    const query = await this.connection.getQueryBuilder(folderMapping);
    await query
      .delete()
      .where("id = :folderId and project_id = :projectId", {
        folderId,
        projectId,
      })
      .execute();
  }

  public async deleteProjectFolders(projectId: string): Promise<void> {
    const query = await this.connection.getQueryBuilder(folderMapping);
    await query
      .delete()
      .where("project_id = :projectId", {
        projectId,
      })
      .execute();
  }

  public async findAllInProject(projectId: string): Promise<Folder[]> {
    const repo = await this.connection.getRepository(folderMapping);

    const result = await repo.find({
      where: {
        projectId,
      },
    });

    return result.map((item) => toFolderEntity(item)) as Folder[];
  }
}
