/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { resolve } from "path";
import { isNullOrUndefined } from "util";
import type { TlsOptions } from "tls";
import { inject, injectable } from "inversify";
import type { Connection, ConnectionOptions, EntitySchema, QueryBuilder, QueryRunner, Repository } from "typeorm";
import { getConnectionManager } from "typeorm";
import { Settings } from "@domain/contracts/settings";
import type { DbConnection } from "@domain/contracts/repositories/db-connection";
import { projectMapping } from "./mappings/project-mapping";
import { folderMapping } from "./mappings/folder-mapping";
import { mockMapping } from "./mappings/mock-mapping";

@injectable()
export class PostgresDbConnection implements DbConnection {
  private connection?: Connection;

  private queryRunner?: QueryRunner;

  private readonly settings: Settings;

  public constructor(@inject(Settings) settings: Settings) {
    this.settings = settings;
  }

  public async close(): Promise<void> {
    if (this.connection?.isConnected === true) {
      await this.connection.close();
    }

    if (this.queryRunner) {
      await this.queryRunner.release();
    }
  }

  public async getRepository<T>(entitySchema: EntitySchema<T>): Promise<Repository<T>> {
    await this.init();

    return this.queryRunner!.manager.getRepository(entitySchema);
  }

  public async getQueryRunner(): Promise<QueryRunner> {
    await this.init();

    return this.queryRunner!;
  }

  public async getConnection(): Promise<Connection> {
    await this.init();

    return this.connection!;
  }

  public async getQueryBuilder<T>(entitySchema: EntitySchema<T>): Promise<QueryBuilder<T>> {
    await this.init();

    return this.queryRunner!.manager.getRepository(entitySchema).createQueryBuilder();
  }

  private createConnection(): Connection {
    const connectionManager = getConnectionManager();

    let ssl: TlsOptions | undefined = undefined;

    if (this.settings.dbSsl) {
      ssl = {
        rejectUnauthorized: false,
      };
    }

    const config: ConnectionOptions = {
      database: this.settings.dbName,
      entities: [projectMapping, folderMapping, mockMapping],
      host: this.settings.dbHost,
      migrations: [resolve(__dirname, "migrations/**.ts")],
      password: this.settings.dbPassword,
      port: this.settings.dbPort,
      ssl,
      type: "postgres",
      username: this.settings.dbUser,
    };

    return connectionManager.has("default") ? connectionManager.get() : connectionManager.create(config);
  }

  private async init(): Promise<void> {
    if (isNullOrUndefined(this.connection)) {
      this.connection = this.createConnection();
    }

    if (!this.connection.isConnected) {
      await this.connection.connect();
      this.queryRunner = undefined;
    }

    if (isNullOrUndefined(this.queryRunner)) {
      this.queryRunner = this.connection.createQueryRunner();
    }
  }
}
