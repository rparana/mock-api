import { inject, injectable } from "inversify";
import type { Repositories, UnitOfWork } from "@domain/contracts/repositories/unit-of-work";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { DbConnection } from "@domain/contracts/repositories/db-connection";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { PostgresDbConnection } from "./postgres-db-connection";

@injectable()
export class PostgresUnitOfWork implements UnitOfWork {
  private readonly dbConnection: PostgresDbConnection;

  private readonly projectRepository: ProjectRepository;

  private readonly folderRepository: FolderRepository;

  public constructor(
    @inject(DbConnection) dbConnection: PostgresDbConnection,
    @inject(ProjectRepository) projectRepository: ProjectRepository,
    @inject(FolderRepository) folderRepository: FolderRepository
  ) {
    this.dbConnection = dbConnection;
    this.projectRepository = projectRepository;
    this.folderRepository = folderRepository;
  }

  public async transaction(callback: (repositories: Repositories) => Promise<void>): Promise<void> {
    const queryRunner = await this.dbConnection.getQueryRunner();

    try {
      await queryRunner.startTransaction();

      await callback({
        folderRepository: this.folderRepository,
        projectRepository: this.projectRepository,
      });

      await queryRunner.commitTransaction();
    } catch (ex) {
      await queryRunner.rollbackTransaction();
      throw ex;
    }
  }
}
