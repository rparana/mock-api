/* eslint-disable no-console */
import { MigrationExecutor } from "typeorm";
import { EnvSettings } from "@infrastructure/env-settings";
import { PostgresDbConnection } from "./postgres-db-connection";

const createExecutor = async (): Promise<MigrationExecutor> => {
  const env = new EnvSettings();
  const dbConnection = new PostgresDbConnection(env);
  const connection = await dbConnection.getConnection();

  return new MigrationExecutor(connection);
};

const execute = async (): Promise<void> => {
  const commands: string[] = ["up", "undo"];

  const runnerArg = process.argv.find((arg: string) => commands.includes(arg));

  if (runnerArg === undefined) {
    throw Error("Invalid arg");
  }

  const executor = await createExecutor();

  if (runnerArg === "up") {
    console.log("RUN PENDING MIGRATIONS");
    await executor.executePendingMigrations();
  } else {
    console.log("UNDO LAST MIGRATIONS");
    await executor.undoLastMigration();
  }
};

const run = async (): Promise<void> => {
  try {
    await execute();
    process.exit(0);
  } catch (ex) {
    console.error(ex);

    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
    process.exit(-1);
  }
};

void run();
