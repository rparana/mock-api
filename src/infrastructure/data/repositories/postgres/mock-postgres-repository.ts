import { inject, injectable } from "inversify";
import type { MockRepository } from "@domain/contracts/repositories/mock-repository";
import type { Mock } from "@domain/entities/mock";
import type { HttpMethod } from "@domain/enums/http-methods";
import { DbConnection } from "@domain/contracts/repositories/db-connection";
import { PostgresDbConnection } from "./postgres-db-connection";
import { mockMapping, toMockDb, toMockEntity } from "./mappings/mock-mapping";

@injectable()
export class MockPostgresRepository implements MockRepository {
  private readonly connection: PostgresDbConnection;

  public constructor(@inject(DbConnection) connection: PostgresDbConnection) {
    this.connection = connection;
  }

  public async getActives(projectId: string, method: HttpMethod): Promise<Mock[]> {
    const repo = await this.connection.getRepository(mockMapping);

    const result = await repo.find({
      where: {
        active: true,
        method,
        projectId,
      },
    });

    return result.map((item) => toMockEntity(item)) as Mock[];
  }

  public async createMock(mock: Mock): Promise<void> {
    const repo = await this.connection.getRepository(mockMapping);

    await repo.save(toMockDb(mock));
  }

  public async deleteMock(projectId: string, mockId: string): Promise<void> {
    const query = await this.connection.getQueryBuilder(mockMapping);
    await query
      .delete()
      .where("id = :mockId and project_id = :projectId", {
        mockId,
        projectId,
      })
      .execute();
  }

  public async editMock(mock: Mock): Promise<void> {
    await this.createMock(mock);
  }

  public async findAllInProject(projectId: string): Promise<Mock[]> {
    const repo = await this.connection.getRepository(mockMapping);

    const result = await repo.find({
      where: {
        projectId,
      },
    });

    return result.map((item) => toMockEntity(item)) as Mock[];
  }

  public async findById(id: string): Promise<Mock | null> {
    const repo = await this.connection.getRepository(mockMapping);

    const result = await repo.findOne(id);

    return toMockEntity(result);
  }

  public async getMock(projectId: string, method: HttpMethod, path: string): Promise<Mock | null> {
    const repo = await this.connection.getRepository(mockMapping);

    const result = await repo.findOne({
      where: {
        method,
        path,
        projectId,
      },
    });

    return toMockEntity(result);
  }
}
