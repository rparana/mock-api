/* eslint-disable @typescript-eslint/ban-types */
import { ResultType } from "@application/enums/result-type";

export class Result<T = Object> {
  public readonly type: ResultType;

  public readonly data: T | null;

  constructor(type: ResultType, data: T | null = null) {
    this.type = type;
    this.data = data;
  }

  public get isEmpty(): boolean {
    return this.data === null;
  }

  public static error(): Result {
    return new Result(ResultType.error);
  }

  public static success<T>(data: T | null = null): Result {
    return new Result(ResultType.success, data);
  }

  public static created<T>(data: T | null = null): Result {
    return new Result(ResultType.created, data);
  }

  public static notfound<T>(data: T | null = null): Result {
    return new Result(ResultType.notFound, data);
  }

  public static invalid<T>(data: T | null = null): Result {
    return new Result(ResultType.invalid, data);
  }

  public static conflict<T>(data: T | null = null): Result {
    return new Result(ResultType.conflict, data);
  }

  public static unauthorized<T>(data: T | null = null): Result {
    return new Result(ResultType.unauthorized, data);
  }
}
