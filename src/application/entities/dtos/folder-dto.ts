import type { Folder } from "@domain/entities/folder";

export class FolderDto {
  public readonly id: string;

  public readonly projectId: string;

  public readonly name: string;

  constructor(folder: Folder) {
    this.id = folder.id;
    this.projectId = folder.projectId;
    this.name = folder.name;
  }
}
