import type { Project } from "@domain/entities/project";

export class ProjectDto {
  public readonly id: string;

  public readonly name: string;

  constructor(project: Project) {
    this.id = project.id;
    this.name = project.name;
  }
}
