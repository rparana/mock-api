import type { MimeTypes } from "@domain/enums/mime-types";
import type { MockResponseType } from "@domain/enums/mock-response-type";
import type { MockResponse } from "@domain/value-objects/mock-response";
import type { Nullable } from "@utils/types";

export class MockResponseDto {
  public readonly status: string;

  public readonly body: Nullable<string>;

  public readonly mimeType: MimeTypes;

  public readonly headers: Nullable<string>;

  public readonly type: MockResponseType;

  public constructor(response: MockResponse) {
    this.type = response.type;
    this.status = response.status;
    this.body = response.body;
    this.mimeType = response.mimeType;
    this.headers = response.headers;
  }
}
