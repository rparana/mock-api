import type { Folder } from "@domain/entities/folder";
import type { Mock } from "@domain/entities/mock";
import { FolderDto } from "./folder-dto";
import { MockDto } from "./mock-dto";

export class FolderMocksDto extends FolderDto {
  public readonly mocks: MockDto[];

  constructor(folder: Folder, mocks: Mock[]) {
    super(folder);
    this.mocks = mocks.map((item) => new MockDto(item));
  }
}
