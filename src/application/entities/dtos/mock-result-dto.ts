import { isNull } from "util";
import type { MimeTypes } from "@domain/enums/mime-types";
import type { MockResponse } from "@domain/value-objects/mock-response";
import type { Dictionary, Nullable } from "@utils/types";
import type { ExecutionSuccess } from "@domain/value-objects/execution-result";

export class MockResultDto {
  public readonly statusCode: number;

  public readonly mimeType: MimeTypes;

  public readonly body: Nullable<unknown>;

  public readonly headers: Nullable<Dictionary>;

  public constructor(statusCode: number, mimeType: MimeTypes, body: Nullable<unknown>, headers: Nullable<Dictionary>) {
    this.statusCode = statusCode;
    this.mimeType = mimeType;
    this.body = body;
    this.headers = headers;
  }

  public static simple(response: MockResponse): MockResultDto {
    return new MockResultDto(
      parseInt(response.status),
      response.mimeType,
      isNull(response.body) ? null : response.body,
      isNull(response.headers) ? null : (JSON.parse(response.headers) as Dictionary)
    );
  }

  public static custom(result: ExecutionSuccess): MockResultDto {
    return new MockResultDto(result.status, result.mimeType, result.body, result.headers);
  }
}
