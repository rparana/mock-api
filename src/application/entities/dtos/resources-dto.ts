import { StatusCodes } from "http-status-codes";
import { orderBy } from "lodash";
import { HttpMethod } from "@domain/enums/http-methods";
import { MimeTypes } from "@domain/enums/mime-types";
import { MockResponseType } from "@domain/enums/mock-response-type";

class StatusCodesDto {
  public readonly status: number;

  public readonly name: string;

  public constructor(status: number, name: string) {
    this.status = status;
    this.name = name;
  }
}

export class ResourcesDto {
  public readonly mimeTypes: string[];

  public readonly mockResponseType: string[];

  public readonly httpMethods: string[];

  public readonly statusCodes: StatusCodesDto[];

  public constructor() {
    this.mimeTypes = this.getValues(MimeTypes);
    this.httpMethods = this.getValues(HttpMethod);
    this.statusCodes = this.getStatusCodes();
    this.mockResponseType = this.getValues(MockResponseType);
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  private getValues(item: {}): string[] {
    const result = Object.entries(item).map(([, value]) => value as string);
    return orderBy(result);
  }

  private getStatusCodes(): StatusCodesDto[] {
    const result = Object.keys(StatusCodes)
      .filter((key) => {
        return isNaN(parseInt(key));
      })
      .map((key) => new StatusCodesDto(StatusCodes[key] as number, key));

    return orderBy(result, (item) => item.status);
  }
}
