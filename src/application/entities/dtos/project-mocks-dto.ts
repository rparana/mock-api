import type { Folder } from "@domain/entities/folder";
import type { Mock } from "@domain/entities/mock";
import type { Project } from "@domain/entities/project";
import { FolderMocksDto } from "./folder-mocks-dto";
import { MockDto } from "./mock-dto";
import { ProjectDto } from "./project-dto";

export class ProjectMocksDto extends ProjectDto {
  public readonly folders: FolderMocksDto[];

  public readonly mocks: MockDto[];

  constructor(project: Project, folders: Folder[], mocks: Mock[]) {
    super(project);
    this.mocks = mocks.filter((mock) => mock.folderId == null).map((mock) => new MockDto(mock));
    this.folders = folders.map((folder) => {
      const folderMocks = mocks.filter((mock) => mock.folderId === folder.id);
      return new FolderMocksDto(folder, folderMocks);
    });
  }
}
