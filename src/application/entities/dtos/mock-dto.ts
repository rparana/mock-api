import type { Mock } from "@domain/entities/mock";
import type { HttpMethod } from "@domain/enums/http-methods";
import type { Nullable } from "@utils/types";
import { MockResponseDto } from "./mock-response-dto";

export class MockDto {
  public readonly id: string;

  public readonly projectId: string;

  public readonly folderId: Nullable<string>;

  public readonly method: HttpMethod;

  public readonly path: string;

  public readonly response: MockResponseDto;

  public readonly active: boolean;

  public constructor(mock: Mock) {
    this.id = mock.id;
    this.projectId = mock.projectId;
    this.folderId = mock.folderId;
    this.method = mock.method;
    this.path = mock.path;
    this.response = new MockResponseDto(mock.response);
    this.active = mock.active;
  }
}
