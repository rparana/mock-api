import { IsString, IsNotEmpty, IsUUID, IsEnum, IsOptional, ValidateNested } from "class-validator";
import { Type } from "class-transformer";
import { Mock } from "@domain/entities/mock";
import { Nullable } from "@utils/types";
import { HttpMethod } from "@domain/enums/http-methods";
import { Input } from "../input";
import { CreateMockResponse } from "./create-mock-response";

export class CreateMock extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly projectId: string;

  @IsUUID()
  @IsOptional()
  public readonly folderId: Nullable<string>;

  @IsEnum(HttpMethod)
  @IsNotEmpty()
  public readonly method: HttpMethod;

  @IsString()
  @IsNotEmpty()
  public readonly path: string;

  @ValidateNested()
  @IsNotEmpty()
  @Type(() => CreateMockResponse)
  public readonly response: CreateMockResponse;

  constructor(
    projectId: string,
    folderId: Nullable<string>,
    method: HttpMethod,
    path: string,
    response: CreateMockResponse
  ) {
    super();
    this.projectId = projectId;
    this.folderId = folderId;
    this.method = method;
    this.path = path;
    this.response = response;
  }

  public toMock(): Mock {
    return new Mock(this.projectId, this.folderId, this.method, this.path, this.response.toMockResponse());
  }
}
