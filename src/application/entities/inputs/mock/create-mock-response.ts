import { IsString, IsNotEmpty, IsEnum, IsOptional } from "class-validator";
import { Nullable } from "@utils/types";
import { MimeTypes } from "@domain/enums/mime-types";
import { MockResponseType } from "@domain/enums/mock-response-type";
import { MockResponse } from "@domain/value-objects/mock-response";

export class CreateMockResponse {
  @IsOptional()
  @IsString()
  public readonly body: Nullable<string>;

  @IsOptional()
  @IsString()
  public readonly headers: Nullable<string>;

  @IsEnum(MimeTypes)
  @IsNotEmpty()
  public readonly mimeType: MimeTypes;

  @IsString()
  @IsNotEmpty()
  public readonly status: string;

  @IsEnum(MockResponseType)
  @IsNotEmpty()
  public readonly type: MockResponseType;

  constructor(
    body: Nullable<string>,
    headers: Nullable<string>,
    mimeType: MimeTypes,
    status: string,
    type: MockResponseType
  ) {
    this.body = body;
    this.headers = headers;
    this.mimeType = mimeType;
    this.status = status;
    this.type = type;
  }

  public toMockResponse(): MockResponse {
    return new MockResponse(this.type, this.status, this.mimeType, this.body, this.headers);
  }
}
