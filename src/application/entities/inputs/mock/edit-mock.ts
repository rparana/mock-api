import { IsString, IsNotEmpty, IsUUID, IsEnum, ValidateNested } from "class-validator";
import { Type } from "class-transformer";
import { HttpMethod } from "@domain/enums/http-methods";
import { Input } from "../input";
import { CreateMockResponse } from "./create-mock-response";

export class EditMock extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly mockId: string;

  @IsEnum(HttpMethod)
  @IsNotEmpty()
  public readonly method: HttpMethod;

  @IsString()
  @IsNotEmpty()
  public readonly path: string;

  @ValidateNested()
  @IsNotEmpty()
  @Type(() => CreateMockResponse)
  public readonly response: CreateMockResponse;

  constructor(mockId: string, method: HttpMethod, path: string, response: CreateMockResponse) {
    super();
    this.mockId = mockId;
    this.method = method;
    this.path = path;
    this.response = response;
  }
}
