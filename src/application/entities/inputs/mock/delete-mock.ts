import { IsString, IsNotEmpty, IsUUID } from "class-validator";
import { Input } from "../input";

export class DeleteMock extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly mockId: string;

  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly projectId: string;

  constructor(mockId: string, projectId: string) {
    super();
    this.mockId = mockId;
    this.projectId = projectId;
  }
}
