import { IsString, IsNotEmpty, IsUUID, IsBoolean } from "class-validator";
import { Input } from "../input";
export class ChangeMockStatus extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly mockId: string;

  @IsBoolean()
  @IsNotEmpty()
  public readonly active: boolean;

  constructor(mockId: string, active: boolean) {
    super();
    this.mockId = mockId;
    this.active = active;
  }
}
