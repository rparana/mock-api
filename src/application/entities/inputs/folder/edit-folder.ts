import { IsString, IsNotEmpty, IsUUID } from "class-validator";
import { Input } from "../input";

export class EditFolder extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly projectId: string;

  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly folderId: string;

  @IsString()
  @IsNotEmpty()
  public readonly newName: string;

  constructor(projectId: string, folderId: string, newName: string) {
    super();
    this.projectId = projectId;
    this.folderId = folderId;
    this.newName = newName;
  }
}
