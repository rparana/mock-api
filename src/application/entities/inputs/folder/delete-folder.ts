import { IsString, IsNotEmpty, IsUUID } from "class-validator";
import { Input } from "../input";

export class DeleteFolder extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly projectId: string;

  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly folderId: string;

  constructor(projectId: string, folderId: string) {
    super();
    this.projectId = projectId;
    this.folderId = folderId;
  }
}
