import { IsString, IsNotEmpty, IsUUID } from "class-validator";
import { Folder } from "@domain/entities/folder";
import { Input } from "../input";

export class CreateFolder extends Input {
  @IsString()
  @IsNotEmpty()
  public readonly name: string;

  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly projectId: string;

  constructor(name: string, projectId: string) {
    super();
    this.name = name;
    this.projectId = projectId;
  }

  public toFolder(): Folder {
    return new Folder(this.projectId, this.name);
  }
}
