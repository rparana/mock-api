import { IsString, IsNotEmpty } from "class-validator";
import { Project } from "@domain/entities/project";
import { Input } from "../input";

export class EditProject extends Input {
  @IsString()
  @IsNotEmpty()
  public readonly id: string;

  @IsString()
  @IsNotEmpty()
  public readonly newName: string;

  constructor(id: string, newName: string) {
    super();
    this.id = id;
    this.newName = newName;
  }

  public toProject(): Project {
    return new Project(this.newName);
  }
}
