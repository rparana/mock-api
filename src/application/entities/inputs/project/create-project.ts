import { IsString, IsNotEmpty } from "class-validator";
import { Project } from "@domain/entities/project";
import { Input } from "../input";

export class CreateProject extends Input {
  @IsString()
  @IsNotEmpty()
  public readonly name: string;

  constructor(name: string) {
    super();
    this.name = name;
  }

  public toProject(): Project {
    return new Project(this.name);
  }
}
