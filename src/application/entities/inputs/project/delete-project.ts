import { IsString, IsNotEmpty, IsUUID } from "class-validator";
import { Input } from "../input";

export class DeleteProject extends Input {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  public readonly id: string;

  constructor(id: string) {
    super();
    this.id = id;
  }
}
