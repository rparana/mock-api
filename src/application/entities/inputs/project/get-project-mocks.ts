import { IsString, IsNotEmpty } from "class-validator";
import { Input } from "../input";

export class GetProjectMocks extends Input {
  @IsString()
  @IsNotEmpty()
  public readonly projectId: string;

  constructor(projectId: string) {
    super();
    this.projectId = projectId;
  }
}
