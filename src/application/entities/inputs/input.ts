import type { ValidationError as ClassValidationError } from "class-validator";
import { validateSync } from "class-validator";
import { ValidationError } from "@domain/validation/validation-error";

export abstract class Input {
  public validate(): [boolean, ValidationError[]] {
    const errors = validateSync(this);

    const validation = errors.map((error) => this.mapError(error));

    return [errors.length > 0, validation];
  }

  private mapError(error: ClassValidationError): ValidationError {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const { value, property, children, constraints } = error;

    return new ValidationError(
      property,
      value,
      constraints,
      children?.map((child) => this.mapError(child))
    );
  }
}
