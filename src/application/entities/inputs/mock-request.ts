import { IsEnum, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { HttpMethod } from "@domain/enums/http-methods";
import type { Dictionary } from "@utils/types";
import { Nullable } from "@utils/types";
import { Input } from "./input";

export class MockRequest extends Input {
  @IsString()
  @IsNotEmpty()
  public readonly projectName: string;

  @IsString()
  @IsNotEmpty()
  public readonly path: string;

  @IsString()
  @IsNotEmpty()
  @IsEnum(HttpMethod)
  public readonly method: HttpMethod;

  @IsOptional()
  public readonly headers: Nullable<Dictionary>;

  @IsOptional()
  public readonly query: Nullable<Dictionary>;

  @IsOptional()
  public readonly body: Nullable<unknown>;

  constructor(
    projectName: string,
    path: string,
    method: HttpMethod,
    headers: Nullable<Dictionary> = null,
    query: Nullable<Dictionary> = null,
    body: Nullable<unknown> = null
  ) {
    super();
    this.projectName = projectName;
    this.path = path;
    this.method = method;
    this.headers = headers;
    this.query = query;
    this.body = body;
  }
}
