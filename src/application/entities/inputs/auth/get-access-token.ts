import { IsString, IsNotEmpty } from "class-validator";
import { Input } from "../input";

export class GetAccessToken extends Input {
  @IsString()
  @IsNotEmpty()
  public readonly code: string;

  @IsString()
  @IsNotEmpty()
  public readonly redirectUri: string;

  constructor(code: string, redirectUri: string) {
    super();
    this.code = code;
    this.redirectUri = redirectUri;
  }
}
