import { injectable } from "inversify";
import { ResourcesDto } from "@application/entities/dtos/resources-dto";
import { Result } from "@application/entities/result";
import { UseCase } from "./use-case";

@injectable()
export class ResourcesUseCase extends UseCase {
  protected async execute(): Promise<Result> {
    const resources = new ResourcesDto();

    return Promise.resolve(Result.success(resources));
  }
}
