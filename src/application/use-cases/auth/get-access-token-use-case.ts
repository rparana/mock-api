import { inject, injectable } from "inversify";
import { Result } from "@application/entities/result";
import type { GetAccessToken } from "@application/entities/inputs/auth/get-access-token";
import { AuthService } from "@domain/contracts/services/auth-service";
import { UseCase } from "../use-case";

@injectable()
export class GetAccessTokenUseCase extends UseCase<GetAccessToken> {
  private readonly authService: AuthService;

  constructor(@inject(AuthService) authService: AuthService) {
    super();
    this.authService = authService;
  }

  protected async execute(param: GetAccessToken): Promise<Result> {
    this.logger.info("Param", { param });

    const token = await this.authService.getAccessToken(param.code, param.redirectUri);

    if (token.isEmpty()) {
      return Result.unauthorized();
    }

    return Result.success(token);
  }
}
