import { inject, injectable } from "inversify";
import type { CreateFolder } from "@application/entities/inputs/folder/create-folder";
import { Result } from "@application/entities/result";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { FolderDto } from "@application/entities/dtos/folder-dto";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { UseCase } from "../use-case";

@injectable()
export class CreateFolderUseCase extends UseCase<CreateFolder> {
  private readonly folders: FolderRepository;

  private readonly projects: ProjectRepository;

  constructor(
    @inject(FolderRepository) folders: FolderRepository,
    @inject(ProjectRepository) projects: ProjectRepository
  ) {
    super();
    this.folders = folders;
    this.projects = projects;
  }

  protected async execute(param: CreateFolder): Promise<Result> {
    this.logger.info("Param", { param });

    const project = await this.projects.findById(param.projectId);

    if (project === null) {
      this.logger.info("Project does not exist");
      return Result.notfound();
    }

    const exists = await this.folders.find(project.id, param.name);

    if (exists !== null) {
      this.logger.info("Folder name already in use in the project");
      return Result.conflict();
    }

    const folder = param.toFolder();

    if (folder.isInvalid()) {
      return Result.invalid(folder.errors);
    }

    await this.folders.createFolder(folder);
    this.logger.info("Folder created");

    return Result.created(new FolderDto(folder));
  }
}
