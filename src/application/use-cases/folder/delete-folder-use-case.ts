import { inject, injectable } from "inversify";
import type { DeleteFolder } from "@application/entities/inputs/folder/delete-folder";
import { Result } from "@application/entities/result";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { UseCase } from "../use-case";

@injectable()
export class DeleteFolderUseCase extends UseCase<DeleteFolder> {
  private readonly folders: FolderRepository;

  constructor(@inject(FolderRepository) folders: FolderRepository) {
    super();
    this.folders = folders;
  }

  protected async execute(param: DeleteFolder): Promise<Result> {
    this.logger.info("Param", { param });

    await this.folders.deleteFolder(param.projectId, param.folderId);

    return Result.success();
  }
}
