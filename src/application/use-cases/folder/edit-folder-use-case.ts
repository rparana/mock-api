import { inject, injectable } from "inversify";
import { Result } from "@application/entities/result";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import type { EditFolder } from "@application/entities/inputs/folder/edit-folder";
import { UseCase } from "../use-case";

@injectable()
export class EditFolderUseCase extends UseCase<EditFolder> {
  private readonly folders: FolderRepository;

  private readonly projects: ProjectRepository;

  constructor(
    @inject(FolderRepository) folders: FolderRepository,
    @inject(ProjectRepository) projects: ProjectRepository
  ) {
    super();
    this.folders = folders;
    this.projects = projects;
  }

  protected async execute(param: EditFolder): Promise<Result> {
    this.logger.info("Param", { param });

    const project = await this.projects.findById(param.projectId);

    if (project === null) {
      this.logger.info("Project does not exist");
      return Result.notfound();
    }

    const folder = await this.folders.findById(param.folderId);

    if (folder === null) {
      this.logger.info("Folder does not exist");
      return Result.notfound();
    }

    folder.changeName(param.newName);

    if (folder.isInvalid()) {
      return Result.invalid(folder.errors);
    }

    const existingFolder = await this.folders.find(param.projectId, param.newName);

    if (existingFolder !== null && existingFolder.id !== folder.id) {
      this.logger.info("Folder name already in use in the project");
      return Result.conflict();
    }

    await this.folders.editFolder(folder);

    return Result.success();
  }
}
