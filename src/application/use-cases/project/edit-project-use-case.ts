import { inject, injectable } from "inversify";
import type { EditProject } from "@application/entities/inputs/project/edit-project";
import { Result } from "@application/entities/result";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { UseCase } from "../use-case";

@injectable()
export class EditProjectUseCase extends UseCase<EditProject> {
  private readonly projects: ProjectRepository;

  constructor(@inject(ProjectRepository) projects: ProjectRepository) {
    super();
    this.projects = projects;
  }

  protected async execute(param: EditProject): Promise<Result> {
    this.logger.info("Param", { param });

    const project = await this.projects.findById(param.id);

    if (project === null) {
      this.logger.info("Project does not exist");
      return Result.notfound();
    }

    project.changeName(param.newName);

    if (project.isInvalid()) {
      return Result.invalid(project.errors);
    }

    const existentProject = await this.projects.find(param.newName);

    if (existentProject !== null && existentProject.id !== project.id) {
      this.logger.info("Name already in use");
      return Result.conflict();
    }

    await this.projects.editProject(project);

    return Result.success();
  }
}
