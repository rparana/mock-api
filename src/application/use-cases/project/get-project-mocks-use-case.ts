import { inject, injectable } from "inversify";
import { Result } from "@application/entities/result";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import type { GetProjectMocks } from "@application/entities/inputs/project/get-project-mocks";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { ProjectMocksDto } from "@application/entities/dtos/project-mocks-dto";
import { UseCase } from "../use-case";

@injectable()
export class GetProjectMocksUseCase extends UseCase<GetProjectMocks> {
  private readonly projectRepository: ProjectRepository;

  private readonly folderRepository: FolderRepository;

  private readonly mockRepository: MockRepository;

  constructor(
    @inject(ProjectRepository) projects: ProjectRepository,
    @inject(FolderRepository) folders: FolderRepository,
    @inject(MockRepository) mocks: MockRepository
  ) {
    super();
    this.projectRepository = projects;
    this.folderRepository = folders;
    this.mockRepository = mocks;
  }

  protected async execute(param: GetProjectMocks): Promise<Result> {
    const project = await this.projectRepository.findById(param.projectId);

    if (project === null) {
      return Result.notfound();
    }

    const folders = await this.folderRepository.findAllInProject(param.projectId);

    const mocks = await this.mockRepository.findAllInProject(param.projectId);

    return Result.success(new ProjectMocksDto(project, folders, mocks));
  }
}
