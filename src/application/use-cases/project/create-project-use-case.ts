import { inject, injectable } from "inversify";
import { ProjectDto } from "@application/entities/dtos/project-dto";
import type { CreateProject } from "@application/entities/inputs/project/create-project";
import { Result } from "@application/entities/result";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { UseCase } from "../use-case";

@injectable()
export class CreateProjectUseCase extends UseCase<CreateProject> {
  private readonly projects: ProjectRepository;

  constructor(@inject(ProjectRepository) projects: ProjectRepository) {
    super();
    this.projects = projects;
  }

  protected async execute(param: CreateProject): Promise<Result> {
    this.logger.info("Param", { param });

    const exists = await this.projects.find(param.name);

    if (exists !== null) {
      this.logger.info("Project name already in use");
      return Result.conflict();
    }

    const project = param.toProject();

    if (project.isInvalid()) {
      return Result.invalid(project.errors);
    }

    await this.projects.createProject(project);
    this.logger.info("Project created");

    return Result.created(new ProjectDto(project));
  }
}
