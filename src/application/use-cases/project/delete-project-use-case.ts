import { inject, injectable } from "inversify";
import type { DeleteProject } from "@application/entities/inputs/project/delete-project";
import { Result } from "@application/entities/result";
import { UnitOfWork } from "@domain/contracts/repositories/unit-of-work";
import { UseCase } from "../use-case";

@injectable()
export class DeleteProjectUseCase extends UseCase<DeleteProject> {
  private readonly unitOfWork: UnitOfWork;

  constructor(@inject(UnitOfWork) unitOfWork: UnitOfWork) {
    super();
    this.unitOfWork = unitOfWork;
  }

  protected async execute(param: DeleteProject): Promise<Result> {
    const projectId = param.id;

    await this.unitOfWork.transaction(async (repos) => {
      const { folderRepository, projectRepository } = repos;
      await folderRepository.deleteProjectFolders(projectId);
      await projectRepository.deleteProject(projectId);
    });

    return Result.success();
  }
}
