import { inject, injectable } from "inversify";
import { Result } from "@application/entities/result";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { ProjectDto } from "@application/entities/dtos/project-dto";
import { UseCase } from "../use-case";

@injectable()
export class GetProjectsUseCase extends UseCase {
  private readonly projectRepository: ProjectRepository;

  constructor(@inject(ProjectRepository) projects: ProjectRepository) {
    super();
    this.projectRepository = projects;
  }

  protected async execute(): Promise<Result> {
    const projects = await this.projectRepository.findAllProjects();

    return Result.success(projects.map((item) => new ProjectDto(item)));
  }
}
