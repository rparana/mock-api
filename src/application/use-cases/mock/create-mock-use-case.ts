import { inject, injectable } from "inversify";
import type { CreateMock } from "@application/entities/inputs/mock/create-mock";
import { Result } from "@application/entities/result";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { MockDto } from "@application/entities/dtos/mock-dto";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { UseCase } from "../use-case";

@injectable()
export class CreateMockUseCase extends UseCase<CreateMock> {
  private readonly folders: FolderRepository;

  private readonly projects: ProjectRepository;

  private readonly mocks: MockRepository;

  constructor(
    @inject(FolderRepository) folders: FolderRepository,
    @inject(ProjectRepository) projects: ProjectRepository,
    @inject(MockRepository) mocks: MockRepository
  ) {
    super();
    this.folders = folders;
    this.projects = projects;
    this.mocks = mocks;
  }

  protected async execute(param: CreateMock): Promise<Result> {
    this.logger.info("Param", { param });

    const project = await this.projects.findById(param.projectId);

    if (project === null) {
      this.logger.info("Project does not exist");
      return Result.notfound();
    }

    if (param.folderId !== null) {
      const folder = await this.folders.findById(param.folderId);

      if (folder === null) {
        this.logger.info("Folder does not exist");
        return Result.conflict();
      }
    }

    const exist = await this.mocks.getMock(param.projectId, param.method, param.path);

    if (exist !== null) {
      this.logger.info("Mock already exists for this project");
      return Result.conflict();
    }

    const mock = param.toMock();

    await this.mocks.createMock(mock);
    this.logger.info("Mock created");

    return Result.created(new MockDto(mock));
  }
}
