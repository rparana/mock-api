import { inject, injectable } from "inversify";
import { Result } from "@application/entities/result";
import type { DeleteMock } from "@application/entities/inputs/mock/delete-mock";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { UseCase } from "../use-case";

@injectable()
export class DeleteMockUseCase extends UseCase<DeleteMock> {
  private readonly mocks: MockRepository;

  constructor(@inject(MockRepository) mocks: MockRepository) {
    super();
    this.mocks = mocks;
  }

  protected async execute(param: DeleteMock): Promise<Result> {
    this.logger.info("Param", { param });

    await this.mocks.deleteMock(param.projectId, param.mockId);

    return Result.success();
  }
}
