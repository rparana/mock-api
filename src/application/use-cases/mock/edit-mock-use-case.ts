import { inject, injectable } from "inversify";
import { Result } from "@application/entities/result";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import type { EditMock } from "@application/entities/inputs/mock/edit-mock";
import { UseCase } from "../use-case";

@injectable()
export class EditMockUseCase extends UseCase<EditMock> {
  private readonly mocks: MockRepository;

  constructor(@inject(MockRepository) mocks: MockRepository) {
    super();
    this.mocks = mocks;
  }

  protected async execute(param: EditMock): Promise<Result> {
    this.logger.info("Param", { param });

    const mock = await this.mocks.findById(param.mockId);

    if (mock === null) {
      this.logger.info("Mock does not exist");
      return Result.notfound();
    }

    const exists = await this.mocks.getMock(mock.projectId, param.method, param.path);

    if (exists !== null && exists.id !== mock.id) {
      return Result.conflict();
    }

    mock.editMock(param.method, param.path, param.response.toMockResponse());

    await this.mocks.editMock(mock);

    return Result.success();
  }
}
