import { inject, injectable } from "inversify";
import type { ChangeMockStatus } from "@application/entities/inputs/mock/change-mock-status";
import { Result } from "@application/entities/result";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { UseCase } from "../use-case";

@injectable()
export class ChangeMockStatusUseCase extends UseCase<ChangeMockStatus> {
  private readonly mocks: MockRepository;

  constructor(@inject(MockRepository) mocks: MockRepository) {
    super();
    this.mocks = mocks;
  }

  protected async execute(param: ChangeMockStatus): Promise<Result> {
    this.logger.info("Param", { param });

    const mock = await this.mocks.findById(param.mockId);

    if (mock === null) {
      this.logger.info("Mock does not exist");
      return Result.notfound();
    }

    mock.changeStatus(param.active);

    await this.mocks.editMock(mock);

    return Result.success();
  }
}
