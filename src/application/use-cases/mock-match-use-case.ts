import { isNullOrUndefined } from "util";
import { inject, injectable } from "inversify";
import type { MockRequest } from "@application/entities/inputs/mock-request";
import { Result } from "@application/entities/result";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { MockResultDto } from "@application/entities/dtos/mock-result-dto";
import { JsRunnerService } from "@domain/services/js-runner-service";
import type { CustomResponseArgs } from "@domain/value-objects/mock-response";
import type { ExecutionSuccess } from "@domain/value-objects/execution-result";
import { UseCase } from "./use-case";

@injectable()
export class MockMatchUseCase extends UseCase<MockRequest> {
  private readonly mocks: MockRepository;

  private readonly projects: ProjectRepository;

  private readonly jsRunner: JsRunnerService;

  public constructor(
    @inject(MockRepository) mockRepository: MockRepository,
    @inject(ProjectRepository) projectRepository: ProjectRepository,
    @inject(JsRunnerService) jsRunner: JsRunnerService
  ) {
    super();
    this.jsRunner = jsRunner;
    this.projects = projectRepository;
    this.mocks = mockRepository;
  }

  protected async execute(param: MockRequest): Promise<Result> {
    this.logger.info("Find project", { name: param.projectName });

    const project = await this.projects.find(param.projectName);
    if (project === null) {
      this.logger.info("Project not found");
      return Result.notfound();
    }

    const mocks = await this.mocks.getActives(project.id, param.method);

    const mock = mocks.find((item) => item.match(param.path));

    if (isNullOrUndefined(mock)) {
      this.logger.info("Mock not found");
      return Result.notfound();
    }

    const { response } = mock;
    if (response.isSimple()) {
      const dto = MockResultDto.simple(response);
      return Result.success(dto);
    }

    const args: CustomResponseArgs = {
      body: param.body,
      headers: param.headers,
      path: mock.extractPathVariables(param.path),
      query: param.query,
    };

    const result = this.jsRunner.execute(response, args);
    if (result.success) {
      const dto = MockResultDto.custom(result as ExecutionSuccess);
      return Result.success(dto);
    }

    return Result.invalid(result);
  }
}
