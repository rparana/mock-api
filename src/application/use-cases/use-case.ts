import { isNullOrUndefined } from "util";
import { inject, injectable } from "inversify";
import { Logger } from "@vizir/simple-json-logger";
import type { Input } from "@application/entities/inputs/input";
import { Result } from "@application/entities/result";

@injectable()
export abstract class UseCase<T extends Input = Input> {
  @inject(Logger)
  protected readonly logger!: Logger;

  public async handler(param?: T): Promise<Result> {
    try {
      if (!isNullOrUndefined(param)) {
        const [isInvalid, errors] = param.validate();

        if (isInvalid) {
          return Result.invalid(errors);
        }
      }

      return await this.execute(param);
    } catch (ex) {
      this.logger.error("usecase error: ", { ex });
      return Result.error();
    }
  }

  protected abstract execute(param?: T): Promise<Result>;
}
