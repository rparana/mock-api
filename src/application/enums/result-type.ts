export enum ResultType {
  success = "success",
  created = "created",
  error = "error",
  notFound = "not_found",
  invalid = "invalid",
  conflict = "conflict",
  unauthorized = "unauthorized",
}
