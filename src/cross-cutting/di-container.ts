import { Container } from "inversify";
import { Logger } from "@vizir/simple-json-logger";
import { ProjectRepository } from "@domain/contracts/repositories/project-repository";
import { MockMatchUseCase } from "@application/use-cases/mock-match-use-case";
import { CreateProjectUseCase } from "@application/use-cases/project/create-project-use-case";
import { EditProjectUseCase } from "@application/use-cases/project/edit-project-use-case";
import { ResourcesUseCase } from "@application/use-cases/resources-use-case";
import { Settings } from "@domain/contracts/settings";
import { EnvSettings } from "@infrastructure/env-settings";
import { PostgresDbConnection } from "@infrastructure/data/repositories/postgres/postgres-db-connection";
import { ProjectPostgresRepository } from "@infrastructure/data/repositories/postgres/project-postgres-repository";
import { CreateFolderUseCase } from "@application/use-cases/folder/create-folder-use-case";
import { FolderRepository } from "@domain/contracts/repositories/folder-repository";
import { FolderPostgresRepository } from "@infrastructure/data/repositories/postgres/folder-postgres-repository";
import { UnitOfWork } from "@domain/contracts/repositories/unit-of-work";
import { PostgresUnitOfWork } from "@infrastructure/data/repositories/postgres/postgres-unit-of-work";
import { DbConnection } from "@domain/contracts/repositories/db-connection";
import { DeleteProjectUseCase } from "@application/use-cases/project/delete-project-use-case";
import { EditFolderUseCase } from "@application/use-cases/folder/edit-folder-use-case";
import { DeleteFolderUseCase } from "@application/use-cases/folder/delete-folder-use-case";
import { MockRepository } from "@domain/contracts/repositories/mock-repository";
import { MockPostgresRepository } from "@infrastructure/data/repositories/postgres/mock-postgres-repository";
import { JsRunnerService } from "@domain/services/js-runner-service";
import { CreateMockUseCase } from "@application/use-cases/mock/create-mock-use-case";
import { GetProjectsUseCase } from "@application/use-cases/project/get-projects-use-case";
import { GetProjectMocksUseCase } from "@application/use-cases/project/get-project-mocks-use-case";
import { EditMockUseCase } from "@application/use-cases/mock/edit-mock-use-case";
import { DeleteMockUseCase } from "@application/use-cases/mock/delete-mock-use-case";
import { ChangeMockStatusUseCase } from "@application/use-cases/mock/change-mock-status-use-case";
import { GetAccessTokenUseCase } from "@application/use-cases/auth/get-access-token-use-case";
import { AuthService } from "@domain/contracts/services/auth-service";
import { GoogleAuthService } from "@infrastructure/services/auth/google-auth-service";
import { MockAuthService } from "@infrastructure/services/auth/mock-auth-service";

export const SERVICE_IDENTIFIER = {
  trackingId: "trackingId",
};

export const createContainer = (): Container => {
  const container = new Container();

  // settings
  const settings = new EnvSettings();
  container.bind(Settings).toConstantValue(settings);

  // repositories
  container.bind(DbConnection).to(PostgresDbConnection).inSingletonScope();
  container.bind(UnitOfWork).to(PostgresUnitOfWork).inSingletonScope();
  container.bind(ProjectRepository).to(ProjectPostgresRepository).inSingletonScope();
  container.bind(FolderRepository).to(FolderPostgresRepository).inSingletonScope();
  container.bind(MockRepository).to(MockPostgresRepository).inSingletonScope();

  // logger
  container.bind(SERVICE_IDENTIFIER.trackingId).toConstantValue("");
  container
    .bind(Logger)
    .toDynamicValue((ctx) => new Logger({ trackingId: ctx.container.get<string>(SERVICE_IDENTIFIER.trackingId) }))
    .inSingletonScope();

  // infrastructure services
  // auth
  if (settings.mockAuth) {
    container.bind(AuthService).to(MockAuthService).inRequestScope();
  } else {
    container.bind(AuthService).to(GoogleAuthService).inSingletonScope();
  }

  // domain
  // service
  container.bind(JsRunnerService).toSelf();

  // use cases
  container.bind(MockMatchUseCase).toSelf();
  container.bind(CreateProjectUseCase).toSelf();
  container.bind(EditProjectUseCase).toSelf();
  container.bind(DeleteProjectUseCase).toSelf();
  container.bind(ResourcesUseCase).toSelf();
  container.bind(CreateFolderUseCase).toSelf();
  container.bind(EditFolderUseCase).toSelf();
  container.bind(DeleteFolderUseCase).toSelf();
  container.bind(CreateMockUseCase).toSelf();
  container.bind(GetProjectsUseCase).toSelf();
  container.bind(GetProjectMocksUseCase).toSelf();
  container.bind(EditMockUseCase).toSelf();
  container.bind(ChangeMockStatusUseCase).toSelf();
  container.bind(DeleteMockUseCase).toSelf();
  container.bind(GetAccessTokenUseCase).toSelf();

  return container;
};
