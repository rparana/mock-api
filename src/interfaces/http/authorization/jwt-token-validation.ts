import type { Request, Response, NextFunction } from "express";
import { StatusCodes } from "http-status-codes";
import { isEmpty } from "lodash";
import type { RequestContainer } from "@utils/types";
import { AuthService } from "@domain/contracts/services/auth-service";

export const jwtTokenValidation = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
  const [, token] = (request.headers["authorization"] ?? "bearer ").split(" ");

  if (isEmpty(token.trim())) {
    response.sendStatus(StatusCodes.UNAUTHORIZED);
    return;
  }

  const req = request as RequestContainer;

  const authService = req.container.get(AuthService);

  const isValidToken = await authService.isValid(token);

  if (!isValidToken) {
    response.sendStatus(StatusCodes.UNAUTHORIZED);
    return;
  }
  next();
};
