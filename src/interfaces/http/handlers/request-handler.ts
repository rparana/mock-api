import type { Response, Request } from "express";
import type { UseCase } from "@application/use-cases/use-case";
import type { Constructor, RequestContainer } from "@utils/types";
import type { Input } from "@application/entities/inputs/input";
import { DbConnection } from "@domain/contracts/repositories/db-connection";
import type { BaseRequest } from "../request/base-request";
import { HttpResponseWrapper } from "../http-response-wrapper";

const requestHandler = (ctorUseCase: Constructor<UseCase>, ctorRequest?: Constructor<BaseRequest<Input>>) => {
  return async (req: Request, res: Response): Promise<void> => {
    const request = req as RequestContainer;

    const useCase = request.container.get<UseCase>(ctorUseCase);

    const data = ctorRequest ? new ctorRequest(request).toData() : undefined;

    const result = await useCase.handler(data);

    const connection = request.container.get(DbConnection);

    await connection.close();

    new HttpResponseWrapper(res).content(result).emit();
  };
};

export { requestHandler };
