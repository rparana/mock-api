import type { Response } from "express";
import type { Result } from "@application/entities/result";
import { MockResultDto } from "@application/entities/dtos/mock-result-dto";
import { parseStatus } from "./parse-status";

export const HEADERS = {
  trackingId: "x-tracking-id",
};

const isMockResult = (result: Result): result is Result<MockResultDto> => {
  return result.data instanceof MockResultDto;
};

export class HttpResponseWrapper {
  private readonly res: Response;

  public constructor(res: Response) {
    this.res = res;
  }

  public content(result: Result): this {
    if (isMockResult(result)) {
      return this.mock(result);
    }

    return this.result(result);
  }

  public emit(): void {
    this.res.end();
  }

  private result(result: Result): this {
    const status = parseStatus[result.type];

    this.res.status(status);

    if (!result.isEmpty) {
      this.res.json(result.data);
    }

    return this;
  }

  private mock(result: Result<MockResultDto>): this {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const data = result.data!;

    this.res.status(data.statusCode);
    this.res.setHeader("Content-Type", data.mimeType);

    if (data.headers !== null) {
      const keys = Object.keys(data.headers);
      for (const key of keys) {
        this.res.setHeader(key, data.headers[key] as string);
      }
    }

    if (data.body !== null) {
      this.res.send(data.body);
    }

    return this;
  }
}
