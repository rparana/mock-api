import type { NextFunction, Response, Request } from "express";
import { v4 } from "uuid";
import type { RequestContainer } from "@utils/types";
import { createContainer, SERVICE_IDENTIFIER } from "@cross-cutting/di-container";
import { HEADERS } from "../http-response-wrapper";

const containerInterceptor = (req: Request, res: Response, next: NextFunction): void => {
  const request = req as RequestContainer;

  const trackingId = request.headers[HEADERS.trackingId] ?? v4();
  request.headers[HEADERS.trackingId] = trackingId;

  const container = createContainer();
  container.rebind(SERVICE_IDENTIFIER.trackingId).toConstantValue(trackingId);

  res.setHeader(HEADERS.trackingId, trackingId);
  request.container = container;

  next();
};

export { containerInterceptor };
