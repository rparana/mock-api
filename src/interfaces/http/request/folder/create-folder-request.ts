import type { Request } from "express";
import { get } from "lodash";
import { CreateFolder } from "@application/entities/inputs/folder/create-folder";
import type { BaseRequest } from "../base-request";

export class CreateFolderRequest implements BaseRequest<CreateFolder> {
  public readonly name: string;

  public readonly projectId: string;

  constructor(req: Request) {
    this.name = get(req, "body.name", "") as string;
    this.projectId = get(req, "body.projectId", "") as string;
  }

  public toData(): CreateFolder {
    return new CreateFolder(this.name, this.projectId);
  }
}
