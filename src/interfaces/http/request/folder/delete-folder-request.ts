import type { Request } from "express";
import { get } from "lodash";
import { DeleteFolder } from "@application/entities/inputs/folder/delete-folder";
import type { BaseRequest } from "../base-request";

export class DeleteFolderRequest implements BaseRequest<DeleteFolder> {
  public readonly projectId: string;

  public readonly folderId: string;

  constructor(req: Request) {
    this.projectId = get(req, "params.projectId", "") as string;
    this.folderId = get(req, "params.folderId", "") as string;
  }

  public toData(): DeleteFolder {
    return new DeleteFolder(this.projectId, this.folderId);
  }
}
