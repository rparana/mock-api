import type { Request } from "express";
import { get } from "lodash";
import { EditFolder } from "@application/entities/inputs/folder/edit-folder";
import type { BaseRequest } from "../base-request";

export class EditFolderRequest implements BaseRequest<EditFolder> {
  public readonly projectId: string;

  public readonly folderId: string;

  public readonly name: string;

  constructor(req: Request) {
    this.projectId = get(req, "body.projectId", "") as string;
    this.folderId = get(req, "params.folderId", "") as string;
    this.name = get(req, "body.name", "") as string;
  }

  public toData(): EditFolder {
    return new EditFolder(this.projectId, this.folderId, this.name);
  }
}
