import type { Request } from "express";
import { get } from "lodash";
import { GetAccessToken } from "@application/entities/inputs/auth/get-access-token";
import type { BaseRequest } from "../base-request";

export class GetAccessTokenRequest implements BaseRequest<GetAccessToken> {
  public readonly code: string;

  public readonly redirectUrl: string;

  constructor(req: Request) {
    this.code = get(req, "query.code", "") as string;
    this.redirectUrl = get(req, "query.redirectUrl", "") as string;
  }

  public toData(): GetAccessToken {
    return new GetAccessToken(this.code, this.redirectUrl);
  }
}
