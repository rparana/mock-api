import type { Request } from "express";
import { get } from "lodash";
import { MockRequest } from "@application/entities/inputs/mock-request";
import type { HttpMethod } from "@domain/enums/http-methods";
import type { Dictionary, Nullable } from "@utils/types";
import type { BaseRequest } from "./base-request";

export class MockRequestWrapper implements BaseRequest<MockRequest> {
  private readonly req: Request;

  constructor(req: Request) {
    this.req = req;
  }

  public get isMock(): boolean {
    return get(this.splitUrl, "[1]", "") === "mock";
  }

  public get project(): string {
    return get(this.splitUrl, "[2]", "") as string;
  }

  public get method(): HttpMethod {
    return this.req.method.toUpperCase() as HttpMethod;
  }

  public get headers(): Nullable<Dictionary> {
    return this.req.headers as Dictionary;
  }

  public get query(): Nullable<Dictionary> {
    return this.req.query;
  }

  public get body(): Nullable<unknown> {
    return this.req.body as unknown;
  }

  public get path(): string {
    return this.req.path.replace(`/mock/${this.project}`, "");
  }

  public toData(): MockRequest {
    return new MockRequest(this.project, this.path, this.method, this.headers, this.query, this.body);
  }

  private get splitUrl(): string[] {
    return this.req.url.split("/");
  }
}
