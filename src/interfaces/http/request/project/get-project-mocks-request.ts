import type { Request } from "express";
import { get } from "lodash";
import { GetProjectMocks } from "@application/entities/inputs/project/get-project-mocks";
import type { BaseRequest } from "../base-request";

export class GetProjectMocksRequest implements BaseRequest<GetProjectMocks> {
  public readonly projectId: string;

  constructor(req: Request) {
    this.projectId = get(req, "params.id", "") as string;
  }

  public toData(): GetProjectMocks {
    return new GetProjectMocks(this.projectId);
  }
}
