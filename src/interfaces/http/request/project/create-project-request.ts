import type { Request } from "express";
import { get } from "lodash";
import { CreateProject } from "@application/entities/inputs/project/create-project";
import type { BaseRequest } from "../base-request";

export class CreateProjectRequest implements BaseRequest<CreateProject> {
  public readonly name: string;

  constructor(req: Request) {
    this.name = get(req, "body.name", "") as string;
  }

  public toData(): CreateProject {
    return new CreateProject(this.name);
  }
}
