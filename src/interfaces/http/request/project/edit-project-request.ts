import type { Request } from "express";
import { get } from "lodash";
import { EditProject } from "@application/entities/inputs/project/edit-project";
import type { BaseRequest } from "../base-request";

export class EditProjectRequest implements BaseRequest<EditProject> {
  public readonly id: string;

  public readonly name: string;

  constructor(req: Request) {
    this.id = get(req, "params.id", "") as string;
    this.name = get(req, "body.name", "") as string;
  }

  public toData(): EditProject {
    return new EditProject(this.id, this.name);
  }
}
