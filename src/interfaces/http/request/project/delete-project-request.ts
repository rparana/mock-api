import type { Request } from "express";
import { get } from "lodash";
import { DeleteProject } from "@application/entities/inputs/project/delete-project";
import type { BaseRequest } from "../base-request";

export class DeleteProjectRequest implements BaseRequest<DeleteProject> {
  public readonly id: string;

  constructor(req: Request) {
    this.id = get(req, "params.id", "") as string;
  }

  public toData(): DeleteProject {
    return new DeleteProject(this.id);
  }
}
