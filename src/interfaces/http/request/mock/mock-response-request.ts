import type { Request } from "express";
import { get } from "lodash";
import { CreateMockResponse } from "@application/entities/inputs/mock/create-mock-response";
import type { Nullable } from "@utils/types";
import type { MimeTypes } from "@domain/enums/mime-types";
import type { MockResponseType } from "@domain/enums/mock-response-type";

export class MockResponseRequest {
  public readonly body: Nullable<string>;

  public readonly headers: Nullable<string>;

  public readonly mimeType: MimeTypes;

  public readonly status: string;

  public readonly type: MockResponseType;

  constructor(req: Request) {
    this.body = get(req, "body.response.body", null) as Nullable<string>;
    this.headers = get(req, "body.response.headers", null) as Nullable<string>;
    this.mimeType = get(req, "body.response.mimeType", "") as MimeTypes;
    this.status = get(req, "body.response.status", "") as string;
    this.type = get(req, "body.response.type", "") as MockResponseType;
  }

  public toData(): CreateMockResponse {
    return new CreateMockResponse(this.body, this.headers, this.mimeType, this.status, this.type);
  }
}
