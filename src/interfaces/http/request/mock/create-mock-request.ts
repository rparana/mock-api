import type { Request } from "express";
import { get } from "lodash";
import { CreateMock } from "@application/entities/inputs/mock/create-mock";
import type { Nullable } from "@utils/types";
import type { HttpMethod } from "@domain/enums/http-methods";
import type { BaseRequest } from "../base-request";
import { MockResponseRequest } from "./mock-response-request";

export class CreateMockRequest implements BaseRequest<CreateMock> {
  public readonly projectId: string;

  public readonly folderId: Nullable<string>;

  public readonly method: HttpMethod;

  public readonly path: string;

  public readonly response: MockResponseRequest;

  constructor(req: Request) {
    this.projectId = get(req, "body.projectId", "") as string;
    this.folderId = get(req, "body.folderId", "") as string;
    this.method = get(req, "body.method", "") as HttpMethod;
    this.path = get(req, "body.path", "") as string;
    this.response = new MockResponseRequest(req);
  }

  public toData(): CreateMock {
    return new CreateMock(this.projectId, this.folderId, this.method, this.path, this.response.toData());
  }
}
