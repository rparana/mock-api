import type { Request } from "express";
import { get } from "lodash";
import type { HttpMethod } from "@domain/enums/http-methods";
import { EditMock } from "@application/entities/inputs/mock/edit-mock";
import type { BaseRequest } from "../base-request";
import { MockResponseRequest } from "./mock-response-request";

export class EditMockRequest implements BaseRequest<EditMock> {
  public readonly mockId: string;

  public readonly method: HttpMethod;

  public readonly path: string;

  public readonly response: MockResponseRequest;

  constructor(req: Request) {
    this.mockId = get(req, "params.mockId", "") as string;
    this.method = get(req, "body.method", "") as HttpMethod;
    this.path = get(req, "body.path", "") as string;
    this.response = new MockResponseRequest(req);
  }

  public toData(): EditMock {
    return new EditMock(this.mockId, this.method, this.path, this.response.toData());
  }
}
