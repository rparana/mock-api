import type { Request } from "express";
import { get } from "lodash";
import { DeleteMock } from "@application/entities/inputs/mock/delete-mock";
import type { BaseRequest } from "../base-request";

export class DeleteMockRequest implements BaseRequest<DeleteMock> {
  public readonly projectId: string;

  public readonly mockId: string;

  constructor(req: Request) {
    this.projectId = get(req, "params.projectId", "") as string;
    this.mockId = get(req, "params.mockId", "") as string;
  }

  public toData(): DeleteMock {
    return new DeleteMock(this.mockId, this.projectId);
  }
}
