import type { Request } from "express";
import { get } from "lodash";
import { ChangeMockStatus } from "@application/entities/inputs/mock/change-mock-status";
import type { BaseRequest } from "../base-request";

export class DeactivateMockRequest implements BaseRequest<ChangeMockStatus> {
  public readonly mockId: string;

  public readonly active: boolean;

  constructor(req: Request) {
    this.mockId = get(req, "params.mockId", "") as string;
    this.active = false;
  }

  public toData(): ChangeMockStatus {
    return new ChangeMockStatus(this.mockId, this.active);
  }
}
