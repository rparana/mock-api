import type { Input } from "@application/entities/inputs/input";

export interface BaseRequest<P extends Input> {
  toData: () => P;
}
