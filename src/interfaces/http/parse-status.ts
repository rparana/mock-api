import { StatusCodes } from "http-status-codes";
import { ResultType } from "@application/enums/result-type";

export const parseStatus: Record<ResultType, number> = {
  [ResultType.success]: StatusCodes.OK,
  [ResultType.created]: StatusCodes.CREATED,
  [ResultType.error]: StatusCodes.INTERNAL_SERVER_ERROR,
  [ResultType.notFound]: StatusCodes.NOT_FOUND,
  [ResultType.invalid]: StatusCodes.UNPROCESSABLE_ENTITY,
  [ResultType.conflict]: StatusCodes.CONFLICT,
  [ResultType.unauthorized]: StatusCodes.UNAUTHORIZED,
};
