import express from "express";
import xmlParser from "express-xml-bodyparser";
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import { EnvSettings } from "@infrastructure/env-settings";
import { CreateProjectUseCase } from "@application/use-cases/project/create-project-use-case";
import { CreateFolderUseCase } from "@application/use-cases/folder/create-folder-use-case";
import { EditProjectUseCase } from "@application/use-cases/project/edit-project-use-case";
import { ResourcesUseCase } from "@application/use-cases/resources-use-case";
import { DeleteProjectUseCase } from "@application/use-cases/project/delete-project-use-case";
import { EditFolderUseCase } from "@application/use-cases/folder/edit-folder-use-case";
import { DeleteFolderUseCase } from "@application/use-cases/folder/delete-folder-use-case";
import { MockMatchUseCase } from "@application/use-cases/mock-match-use-case";
import { CreateMockUseCase } from "@application/use-cases/mock/create-mock-use-case";
import { GetProjectsUseCase } from "@application/use-cases/project/get-projects-use-case";
import { GetProjectMocksUseCase } from "@application/use-cases/project/get-project-mocks-use-case";
import { EditMockUseCase } from "@application/use-cases/mock/edit-mock-use-case";
import { DeleteMockUseCase } from "@application/use-cases/mock/delete-mock-use-case";
import { ChangeMockStatusUseCase } from "@application/use-cases/mock/change-mock-status-use-case";
import { GetAccessTokenUseCase } from "@application/use-cases/auth/get-access-token-use-case";
import swagger from "../../../mock-api-swagger.json";
import { containerInterceptor } from "./interceptors/container-interceptor";
import { requestHandler } from "./handlers/request-handler";
import { CreateProjectRequest } from "./request/project/create-project-request";
import { CreateFolderRequest } from "./request/folder/create-folder-request";
import { EditProjectRequest } from "./request/project/edit-project-request";
import { DeleteProjectRequest } from "./request/project/delete-project-request";
import { EditFolderRequest } from "./request/folder/edit-folder-request";
import { DeleteFolderRequest } from "./request/folder/delete-folder-request";
import { MockRequestWrapper } from "./request/mock-request-wrapper";
import { CreateMockRequest } from "./request/mock/create-mock-request";
import { GetProjectMocksRequest } from "./request/project/get-project-mocks-request";
import { EditMockRequest } from "./request/mock/edit-mock-request";
import { DeleteMockRequest } from "./request/mock/delete-mock-request";
import { ActivateMockRequest } from "./request/mock/activate-mock-status-request";
import { DeactivateMockRequest } from "./request/mock/deactivate-mock-status-request";
import { GetAccessTokenRequest } from "./request/auth/get-access-token-request";
import { jwtTokenValidation } from "./authorization/jwt-token-validation";

export class Server {
  public static run(): express.Application {
    const app = express();

    app.use(cors());

    app.use(express.json());
    app.use(express.text());
    app.use(
      xmlParser({
        explicitArray: false,
        trim: false,
      })
    );

    app.use(containerInterceptor);

    // auth
    app.get("/google/authenticated", requestHandler(GetAccessTokenUseCase, GetAccessTokenRequest));

    // mock
    app.all("/mock/*", requestHandler(MockMatchUseCase, MockRequestWrapper));

    const env = new EnvSettings();
    if (env.enabledSwagger) {
      app.use("/docs", swaggerUi.serve, swaggerUi.setup(swagger));
    }

    app.use(jwtTokenValidation);

    const routes = this.registerRoutes();
    app.use("/manage", routes);

    return app;
  }

  private static registerRoutes(): express.Router {
    const router = express.Router();

    /* eslint-disable sonarjs/no-duplicate-string */
    // Resources
    router.get("/resources", requestHandler(ResourcesUseCase));

    // Project
    router.post("/project", requestHandler(CreateProjectUseCase, CreateProjectRequest));
    router.put("/project/:id", requestHandler(EditProjectUseCase, EditProjectRequest));
    router.delete("/project/:id", requestHandler(DeleteProjectUseCase, DeleteProjectRequest));
    router.get("/project", requestHandler(GetProjectsUseCase));
    router.get("/project/:id", requestHandler(GetProjectMocksUseCase, GetProjectMocksRequest));

    // Folder
    router.post("/folder", requestHandler(CreateFolderUseCase, CreateFolderRequest));
    router.put("/folder/:folderId", requestHandler(EditFolderUseCase, EditFolderRequest));
    router.delete("/project/:projectId/folder/:folderId", requestHandler(DeleteFolderUseCase, DeleteFolderRequest));

    // Mock
    router.post("/mock", requestHandler(CreateMockUseCase, CreateMockRequest));
    router.put("/mock/:mockId", requestHandler(EditMockUseCase, EditMockRequest));
    router.put("/mock/:mockId/activate", requestHandler(ChangeMockStatusUseCase, ActivateMockRequest));
    router.put("/mock/:mockId/deactivate", requestHandler(ChangeMockStatusUseCase, DeactivateMockRequest));
    router.delete("/project/:projectId/mock/:mockId", requestHandler(DeleteMockUseCase, DeleteMockRequest));
    /* eslint-enable sonarjs/no-duplicate-string */

    return router;
  }
}
